<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'healthcare-trivi' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         ':3~O9RM[`SY5~qx4MMXPg8%/EU%t6MdQdr4BhRzsw)><J>CXgo;Cu{mK<1BoM-(e' );
define( 'SECURE_AUTH_KEY',  'o;ICkxQ>H@L+!G[Rq,<su6XE8u[$WL.o8wE|jOFhtdK.UQ;.H&6Hena t4 #@y<C' );
define( 'LOGGED_IN_KEY',    '4I%H4Z@[ch#0y.Pt)~y9d4a N+46J{quS}Zs9A?`A4Ug9Y4%h)Y~1l@{6Yz~H$?s' );
define( 'NONCE_KEY',        'p<6c1hsM`F57l`n[6EW0TSOjG|NC1f?N,p5T?^_RKHq+;R@(29d0o=0GPus@6U~e' );
define( 'AUTH_SALT',        '[{&uyj9T 2weU]z?A@!?WfRAc{.B{oBk<Ug(z{IE%IS;&d2P0=[UzyY)0q_!a.qa' );
define( 'SECURE_AUTH_SALT', 'T~0,KpFU(T;oEe$a-TI9K#m/KAKH`?$j*`(cY8z~e5m?_#KbwiE4iBb1W0Mta28m' );
define( 'LOGGED_IN_SALT',   'bK:Ow@Qr+-e{[i%`]V?a~N&6N#Nx2{ccP2SU>VJ#2)Ie-<uBTUP}]-+P&H<tuY8V' );
define( 'NONCE_SALT',       'rkdR3;#w}dDTvID#B@s)rb2PMRV{~4QgC1|%_m[5U,N-]EGZQRj&=-;ov2M)xG[3' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
