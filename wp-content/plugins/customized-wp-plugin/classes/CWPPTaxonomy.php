<?php

/**
 * Taxonomy Wrapper Class
 */

defined( 'CWPP_VERSION' ) or die( 'No script kiddies please!');

class CWPPTaxonomy{

    protected $registered = false;

    public
        $labelName = NULL,
        $labelNameSingular = NULL,
        $rewrite = true,
        $rewriteSlug = NULL,
        $rewriteWithFront = true,
        $showUI = true,
        $queryVar = NULL,
        $showAdminColumn = true,
        $postTypes = array(),
        $hierarchical = false
    ;

    public function __construct(){

    }

    public function registerTaxonomy(){
        if( $this->registered ) return;
        $class = get_class( $this );

        if( empty( $this->labelName ) ) $this->labelName = ucfirst( $class );
        if( empty( $this->labelNameSingular ) ) $this->labelNameSingular = $this->labelName;
        if( empty( $this->rewriteSlug ) ) $this->rewriteSlug = $class;

        $labels = array(
            'name' => $this->labelName,
            'label' => $this->labelName,
            'singular_name' => $this->labelNameSingular,
            'all_items' => __( 'All ' . $this->labelName, CWPP_THEME_TEXT_DOMAIN ),
            'edit_item' => __( 'Edit ' . $this->labelNameSingular, CWPP_THEME_TEXT_DOMAIN ),
            'view_item' => __( 'View ' . $this->labelNameSingular, CWPP_THEME_TEXT_DOMAIN ),
            'update_item' => __( 'Update ' . $this->labelNameSingular, CWPP_THEME_TEXT_DOMAIN ),
            'add_new_item' => __( 'Add New ' . $this->labelNameSingular, CWPP_THEME_TEXT_DOMAIN ),
            'new_item_name' => __( 'Add ' . $this->labelNameSingular . ' Name', CWPP_THEME_TEXT_DOMAIN ),
            'parent_item' => __( 'Parent ' . $this->labelNameSingular, CWPP_THEME_TEXT_DOMAIN ),
            'parent_item_colon' => __( 'Parent ' . $this->labelNameSingular .':', CWPP_THEME_TEXT_DOMAIN ),
            'search_items' => __( 'Search ' . $this->labelName, CWPP_THEME_TEXT_DOMAIN ),
            'popular_items' => __( 'Popular ' . $this->labelName, CWPP_THEME_TEXT_DOMAIN ),
            'separate_items_with_commas' => __( 'Separate ' . $this->labelName . 'with commas', CWPP_THEME_TEXT_DOMAIN ),
            'add_or_remove_items' => __( 'Add or remove ' . $this->labelName, CWPP_THEME_TEXT_DOMAIN ),
            'choose_from_most_used' => __( 'Choose from the most used ' . $this->labelName, CWPP_THEME_TEXT_DOMAIN ),
            'not_found' => __( 'No ' . $this->labelName . ' found ', CWPP_THEME_TEXT_DOMAIN )
        );

        $this->queryVar = empty( $this->queryVar ) ? $class : $this->queryVar;
        $rewrite = $this->rewrite ? array("slug" => $this->rewriteSlug, "with_front" => $this->rewriteWithFront) : false;
        $args = array(
            'labels' => $labels,
            'hierarchical' => $this->hierarchical,
            'label' => $this->labelName,
            'show_ui' => $this->showUI,
            'query_var' => $this->queryVar,
            'rewrite' => $rewrite,
            'show_admin_column' => $this->showAdminColumn,
        );
        register_taxonomy( $class, $this->postTypes, $args );
        $this->registered = true;
    }
}