<?php

/**
 * The Core Application Framework
 */


defined( 'CWPP_VERSION' ) or die( 'No script kiddies please!');

class CustomizedWPPlugin{

    protected $config, $styles, $scripts, $emailType;
    public $forms;
    public static $ajaxActions;

    public function __construct( $configFile ){
        // === Load Configurations And Process It ====
        if( file_exists( $configFile ) ){
            $this->config = require_once( $configFile );
            $this->processConfig();
        }

        add_action( 'init', array($this, 'loadPostTypes'));
        add_action( 'init', array($this, 'loadTaxonomies'));
        add_action( 'init', array($this, 'loadForms'));

        // === Load Generic Functions ====
        $this->loadGenericFunctions();

        // === Load And Run CWPP Ajax ====
        $this->ajaxLoader();
        $this->ajaxRun();

    }

    public function loadForms(){
        $dir = CWPP_FORMS_DIR . '/';
        if( !file_exists( $dir ) ) return;
        $files = glob( $dir . "*.php");
        if( !empty( $files ) ){
            foreach( $files as $file ){
                $formName = substr( basename( $file ), 0, -4 );
                $config = require_once( $file );

                $this->forms[$formName] = new CWPPForm( $config, $formName );

            }
        }
    }

    public function loadPostTypes(){
        $dir = CWPP_POST_TYPES_DIR . '/';
        if( !file_exists( $dir ) ) return;
        $files = glob( $dir . "*.php");
        if( !empty( $files ) ){
            foreach( $files as $file ){
                $className = substr( basename( $file ), 0, -4 );
                require_once( $file );
                if( class_exists( $className ) ){
                    $obj = new $className();
                    $obj->registerPostType();
                }
            }
        }
    }

    public function loadTaxonomies(){
        $dir = CWPP_TAXONOMIES_DIR . '/';
        if( !file_exists( $dir ) ) return;
        $files = glob( $dir . "*.php");
        if( !empty( $files ) ){
            foreach( $files as $file ){
                $className = substr( basename( $file ), 0, -4 );
                require_once( $file );
                if( class_exists( $className ) ){
                    $obj = new $className();
                    $obj->registerTaxonomy();
                }
            }
        }
    }

    public function loadGenericFunctions(){
        $file = CWPP_THEME_MOD_DIR .'/functions.php';
        if( file_exists( $file ) ) require_once( $file );
    }

    protected function ajaxLoader(){
        $dir = CWPP_THEME_MOD_DIR . '/ajax/';
        if( !file_exists( $dir ) ) return;
        $files = glob( $dir . "*.php");
        $actions = &self::$ajaxActions;
        if( !empty( $files )  ){
            foreach( $files as $file ){
                $actionName = substr( basename( $file ), 0, -4 );
                $actions[ $actionName ] = $file;
                add_action( 'cwpp_ajax_' . $actionName, function( $actionName ){
                    if( isset( self::$ajaxActions[ $actionName ] ) ){
                        require_once( self::$ajaxActions[ $actionName ] );
                    }
                }, 10, 1);
            }
        }
    }

    public static function header() { ?>
        <script>

            if( !cwpp ) var cwpp = {
                functions: {
                    list: [],

                    register: function( name, callback ){
                        cwpp.functions.list[ name ] = callback;
                    },

                    deRegister: function( name ){
                        cwpp.functions.list[ name ] = null;
                        cwpp.functions.list[ name ] = undefined;
                    }

                },

                ajax: {
                    url: '<?php echo CWPP_AJAX_URI; ?>',
                },

                forms: {}
            };

            window.cwpp = cwpp;

        </script>
    <?php }

    public static function footer() { ?>
        <script>
            /* Ajax Forms */
            cwpp.forms.register = function(){
                (function( $ ){
                    $('form.cwpp-ajax').each(function(i,e){
                        var $form = $(e);
                        var $inputs = $form.find('input,select,button,textarea');
                        var $output = $form.find('.output');
                        var isMultipart = false;
                        if($form.attr('enctype') && $form.attr('enctype').toLowerCase() == 'multipart/form-data'){
                            isMultipart = true;
                        }
                        if (typeof $form.attr('id') == 'undefined' || !$form.attr('id')){
                            $form.attr('id', 'cwpp-ajax-form-' + i);
                        }
                        $output.hide();
                        $inputs.change(function(e){
                            $(this).removeClass('input-error');
                        });

                        if(isMultipart){
                            var frameId = 'cwpp-ajax-frame-' + i;
                            var $frame = $('<iframe src="#"></iframe>') ;
                            var $inp = $('<input name="cwpp-form-id" type="hidden" />');

                            $inp.val( $form.attr('id') );

                            $frame
                                .attr('id', frameId )
                                .attr('name', frameId )
                                .css('display', 'none');

                            $form
                                .attr('action', cwpp.ajaxURL)
                                .append($inp)
                                .append($frame)
                                .attr('target', frameId);
                        }

                        $form.submit( function(e){

                            var cb_pre_ajax = $form.data('pre-ajax');
                            if( typeof cb_pre_ajax != 'undefined' && typeof cwpp.functions.list[ cb_pre_ajax ] != 'undefined' ){
                                cb_pre_ajax = cwpp.functions.list[ cb_pre_ajax ];
                                if( cb_pre_ajax( $form, e ) === false ){
                                    e.preventDefault();
                                    return;
                                }
                            }

                            if( !isMultipart ){

                                e.preventDefault();

                                var form_data = $form.serialize();

                                $.post( cwpp.ajax.url, form_data, function(data){
                                    cwpp.forms.output( $form, $output, data );
                                }).always(function(){
                                    $inputs.prop('disabled', false);
                                    $form.removeClass( 'form-state-loading' );
                                });
                            }

                            $form.addClass( 'form-state-loading' );

                            $form.find( '.input-error' ).removeClass( 'input-error' );
                            setTimeout(function(){
                                $inputs.prop( 'disabled', true );
                            }, 10);
                            $form.removeClass( 'form-state-error form-state-success' );
                            $output.slideUp();

                        });
                    });
                })(jQuery);
            };

            cwpp.forms.output = function( form, output, data ){
                (function( $ ){
                    var $form = $(form);
                    var $output = $(output);
                    var $inputs = $form.find('input,select,button,textarea');
                    if( data.error ){
                        $form.addClass('form-state-error');
                        for( i in data.error_fields ){
                            var field = data.error_fields[i];
                            $('#' + field).addClass('input-error');
                            $('#label_' + field).addClass('input-error');
                        }
                    }else{
                        $form.addClass('form-state-success');
                        $form.find('[data-reset]').val('');

                    }
                    if(typeof data.callback != 'undefined' && typeof cwpp.functions.list[ data.callback ] == 'function' ){
                        cwpp.functions.list[ data.callback ](form, data);
                    }

                    $form.removeClass( 'form-state-loading' );
                    $inputs.prop('disabled', false);
                    $output.html( data.msg );
                    $output.slideDown();

                    var cb_post_ajax = $form.data('post-ajax');
                    if( typeof cb_post_ajax != 'undefined' && typeof cwpp.functions.list[ cb_post_ajax ] != 'undefined' ){
                        cb_post_ajax = cwpp.functions.list[ cb_post_ajax ];
                        if( cb_post_ajax( $form, data ) === false ) return;
                    }

                })(jQuery);
            }

            jQuery(document).ready(function(){
                cwpp.forms.register();
            });

//            console.log( cwpp );

        </script>
    <?php }


    /**
     * Setup CWPP Ajax System. Attach any ajax using action parameter
     * Use cwpp.ajax.url global variable in JS to access ajax url
     */
    protected function ajaxRun(){
        add_action('wp_head', array( get_class($this), 'header') , 1 );
        add_action('admin_head', array( get_class($this), 'header') , 1 );
        add_action('wp_footer', array( get_class($this), 'footer') , 99999999 );
        add_action('admin_footer', array( get_class($this), 'footer') , 99999999 );

        add_action('init', function (){
            if( isset( $_REQUEST['cwpp_ajax'] ) ){
                if( isset( $_REQUEST['action'] ) AND !empty( $_REQUEST['action'] ) ){
                    send_nosniff_header();
                    nocache_headers();

                    do_action( 'cwpp_ajax_' . $_REQUEST['action'], $_REQUEST['action'] );
                    exit;
                }
                wp_die("Sorry, That's not possible", 'ERROR');
            }
        }, 10 );
    }

    protected function processConfig(){

//        $this->isThemeUpdated();

        // Styles ====
        $this->doStyles();
        // Scripts ====
        $this->doScripts();

        // Email ===
        $this->doEmail();

        // Enqueue Styles ====
        add_action( 'wp_enqueue_scripts', array($this, 'doEnqueueStyles'));
        // Enqueue Scripts ====
        add_action( 'wp_enqueue_scripts', array($this, 'doEnqueueScripts'));

        // Image Sizes ====
        add_action( 'after_setup_theme', array($this, 'doImageSizes'));
        // Sidebars ====
        add_action( 'widgets_init', array($this, 'doSidebars'));
        // Menus ====
        add_action( 'after_setup_theme', array($this, 'doMenus'));
        // Theme Supports ====
        add_action( 'after_setup_theme', array($this, 'doThemeSupports'));
        // Widgets ====
        add_action( 'widgets_init', array($this, 'doWidgets'));
    }

    protected static function fileCheckLocal( &$path ){
        if( !( substr( $path, 0, 2 ) === '//' OR substr( $path, 0, 4 ) === 'http' ) ){
            $path = ltrim( $path, '/' );
            return true;
        }
        return false;
    }


    public function doEnqueueStyles(){
        if( isset( $this->styles['enqueue'] ) AND !empty( $this->styles['enqueue'] ) ){
            foreach( $this->styles['enqueue'] as $item ){
                list( $identifier, $src, $fileLocal ) = $item;
                if( $fileLocal ) $src = CWPP_THEME_URI . "/$src";
                wp_enqueue_style( $identifier, $src, NULL, CWPP_THEME_VERSION );
            }
        }
    }

    public function doEnqueueScripts(){
        wp_deregister_script( 'jquery' );
        if( isset( $this->scripts['enqueue'] ) AND !empty( $this->scripts['enqueue'] ) ){
            foreach( $this->scripts['enqueue'] as $item ){
                list( $identifier, $src, $fileLocal ) = $item;
                if( $fileLocal ) $src = CWPP_THEME_URI . "/$src";
                wp_enqueue_script( $identifier, $src, NULL, CWPP_THEME_VERSION, true );
            }
        }
    }

    public function doStyles(){
        if( !isset( $this->config[ CWPP_CFG_STYLES ] ) OR !is_array( $this->config[ CWPP_CFG_STYLES ] ) ) return;
        foreach($this->config[ CWPP_CFG_STYLES ] as $key => $src ){
            $fileLocal = $this->fileCheckLocal( $src );
            $this->styles['enqueue'][] = array($key, $src, $fileLocal);
        }
    }

    public function doScripts(){
        if( !isset( $this->config[ CWPP_CFG_SCRIPTS ] ) OR !is_array( $this->config[ CWPP_CFG_SCRIPTS ] ) ) return;
        foreach($this->config[ CWPP_CFG_SCRIPTS ] as $key => $src ){
            $fileLocal = $this->fileCheckLocal( $src );
            $this->scripts['enqueue'][] = array($key, $src, $fileLocal);
        }
    }

    public function doImageSizes(){
        if( !isset( $this->config[ CWPP_CFG_IMAGE_SIZES ] ) OR !is_array( $this->config[ CWPP_CFG_IMAGE_SIZES ] ) ) return;
        foreach($this->config[ CWPP_CFG_IMAGE_SIZES ] as $key => $definition ){
            if( !is_array( $definition ) OR count( $definition ) < 1 ) continue;
            list( $width, $height, $crop ) = $definition;
            add_image_size( $key, $width, $height, $crop );
        }
    }

    public function doSidebars(){
        if( !isset( $this->config[ CWPP_CFG_SIDEBARS ] ) OR !is_array( $this->config[ CWPP_CFG_SIDEBARS ] ) ) return;
        foreach($this->config[ CWPP_CFG_SIDEBARS ] as $key => $definition ){
            if( !is_array( $definition ) OR count( $definition ) < 1 ) continue;
            list( $name, $description, $class, $args ) = $definition;
            $args = wp_parse_args( array(
                'name' => __( $name, CWPP_THEME_TEXT_DOMAIN ),
                'id' => $key,
                'description' => __( $description, CWPP_THEME_TEXT_DOMAIN ),
                'class' => $class,
                'before_widget' => '<div id="%1$s" class="widget %2$s">',
                'after_widget' => '</div>',
            ), $args );
            register_sidebar( $args );
        }
    }

    public function doMenus(){
        if( !isset( $this->config[ CWPP_CFG_MENUS ] ) OR !is_array( $this->config[ CWPP_CFG_MENUS ] ) ) return;
        register_nav_menus( $this->config[ CWPP_CFG_MENUS ] );
    }

    public function doThemeSupports(){
        if( !isset( $this->config[ CWPP_CFG_THEME_SUPPORTS ] ) OR !is_array( $this->config[ CWPP_CFG_THEME_SUPPORTS ] ) ) return;
        foreach($this->config[ CWPP_CFG_THEME_SUPPORTS ] as $key => $definition ){
            if( $definition === true ) add_theme_support( $key );
            else add_theme_support( $key, $definition );
        }
    }

    public function doWidgets(){
        $dir = CWPP_WIDGETS_DIR . '/';
        if( !file_exists( $dir ) ) return;
        $files = glob( $dir . "*.php");
        if( !empty( $files )  ){
            foreach( $files as $file ){
                $className = substr( basename( $file ), 0, -4 );
                require_once( $file );
                if( class_exists( $className ) ){
                    register_widget( $className );
                }
            }
        }
    }

    public function doEmail(){
        $this->emailType = CWPP_EMAIL_TYPE_PLAIN;
        if( isset( $this->config[ CWPP_EMAIL ] ) AND is_array( $this->config[ CWPP_EMAIL ] ) ){
            if( isset( $this->config[ CWPP_EMAIL ]['type'] ) ){
                $this->emailType = $this->config[ CWPP_EMAIL ]['type'];
            }
        }

        // Email Configuration:
        if(
            isset( $this->config[ CWPP_CFG_EMAIL ] ) AND
            is_array( $this->config[ CWPP_CFG_EMAIL ] )
        ){

            $emailConfig = $this->config[ CWPP_CFG_EMAIL ];

            if(
                isset( $emailConfig[ CWPP_CFG_EMAIL_METHOD ] ) AND
                $emailConfig[ CWPP_CFG_EMAIL_METHOD ] == CWPP_EMAIL_METHOD_SMTP
            ){
                add_action( 'phpmailer_init', function( $phpmailer ) use ($emailConfig){
                    /** @var PHPMailer $phpmailer */
                    $phpmailer->isSMTP();
                    $phpmailer->SMTPDebug = true;
                    $phpmailer->Host = isset( $emailConfig[ CWPP_CFG_EMAIL_SMTP_HOST ] ) ? $emailConfig[ CWPP_CFG_EMAIL_SMTP_HOST ] : '';
                    $phpmailer->Port = isset( $emailConfig[ CWPP_CFG_EMAIL_SMTP_PORT ] ) ? $emailConfig[ CWPP_CFG_EMAIL_SMTP_PORT ] : '';

                    if( !empty( $emailConfig[ CWPP_CFG_EMAIL_SMTP_AUTH ] ) ){
                        $phpmailer->SMTPAuth = true;
                        $auth = $emailConfig[ CWPP_CFG_EMAIL_SMTP_AUTH ];
                        $phpmailer->Username = isset( $auth[ CWPP_CFG_EMAIL_SMTP_AUTH_USER ] ) ? $auth[ CWPP_CFG_EMAIL_SMTP_AUTH_USER ] : '';
                        $phpmailer->Password = isset( $auth[ CWPP_CFG_EMAIL_SMTP_AUTH_PASS ] ) ? $auth[ CWPP_CFG_EMAIL_SMTP_AUTH_PASS ] : '';
                    }

                    $phpmailer->SMTPSecure = isset( $auth[ CWPP_CFG_EMAIL_SMTP_SECURE ] ) ? $auth[ CWPP_CFG_EMAIL_SMTP_SECURE ] : false;
                });
            }

        }

    }

    public function getEmailType(){
        return $this->emailType;
    }
}