<?php

defined( 'CWPP_VERSION' ) or die('No script kiddies please!');

class CWPPAjaxForm{
    protected $errorFields, $mandatoryFields, $dataArray, $message, $hasError;
    public $formId;
    public function __construct( &$dataArray, $mandatoryFields = array(), $validators ){
        $this->errorFields = array();
        $this->mandatoryFields = is_array( $mandatoryFields ) ? $mandatoryFields : array();
        $this->mandatoryFields = array_fill_keys( array_values( $this->mandatoryFields ), true );
        $this->dataArray = (array) $dataArray;
        $this->errorState = false;
        $this->message = '';
        $this->validators = is_array( $validators ) ? $validators : array();

        if(isset($dataArray['cwpp-form-id'])){
            $this->formId = $dataArray['cwpp-form-id'];

            if(!(preg_match('/^[a-z0-9_\-]+$/i', $this->formId))){
                $this->formId = '';
            }
            unset($dataArray['cwpp-form-id']);
        }
    }

    public function validateFields(){
        foreach( $this->mandatoryFields as $key => $val ){
            if( empty( $this->dataArray[ $key ] ) ) $this->errorFields[ $key ] = true;
        }
        foreach( $this->dataArray as $key => $val ){
            if( $key == 'action' ) continue;
            if( $val != '' AND isset( $this->validators[ $key ] ) ){
                if( !$this->validators[ $key ]( $val ) ) $this->errorFields[ $key ] = true;
            }
        }
        if( count( $this->errorFields ) ){
            $this->hasError = true;
            return false;
        }
        return true;
    }

    public function setErrorField( $field ){
        $this->hasError = true;
        $this->errorFields[ $field ] = true;
    }

    public function hasError(){
        return $this->hasError;
    }

    public function error( $message ){
        $this->hasError = true;
        $this->message = $message;
        return true;
    }

    public function success( $message, $forceOverwrite = false ){
        if( $forceOverwrite OR !$this->hasError() ){
            $this->hasError = false;
            $this->message = $message;
            return true;
        }
        return false;
    }


    /**
     * @param string $contentType
     * @return array|null
     */
    public function response( $contentType = 'json' ){

        $out = array(
            'error' => $this->hasError(),
            'error_fields' => array_keys( $this->errorFields ),
            'msg' => $this->message,
        );


        switch( $contentType ){

            case CWPP_AJAX_RESPONSE_JSON:
                header('Content-Type: application/json');
                echo json_encode( $out );
                break;

            case CWPP_AJAX_RESPONSE_SCRIPT: ?>
                <script>
                    window.parent.cwpp.forms.output(
                        '#<?php echo $this->formId; ?>',
                        '.output',
                        <?php echo json_encode( $out ); ?>
                    );
                </script>
            <?php
                break;

            default:
                return $out;
        }

        return NULL;
    }
}
