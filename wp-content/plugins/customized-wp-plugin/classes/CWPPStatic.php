<?php

class CWPPStatic{

    public static $currentForm;

    /**
     * Slugify a string. Uses 'sanitize_title' API of WordPress
     * @param $str
     * @param string $sep
     * @return mixed|string
     */
    public static function slugify($str, $sep = '-'){

        $str = sanitize_title( $str );
        if( $sep != '-' ){
            $str = str_replace( '-', $sep, $str );
        }

        return $str;
    }


    /**
     * Get Featured Image of any post.
     * @param int $postId The Post Id. Defaults to Current Post
     * @param null|string $size Size of Image to retrieve. Defaults to 'thumbnail'
     * @param null|string|mixed $default Returns this if image not found.
     * @return null|string|mixed
     */
    public static function getFeaturedImage( $postId = 0, $size = NULL, $default = NULL ){
        if( has_post_thumbnail( $postId ) ){
            global $post;
            if( !$postId ) $postId = $post->ID;
            if( !$size ) $size = 'thumbnail';
            $image = wp_get_attachment_image_src( get_post_thumbnail_id( $postId ), $size );
            if( $image ) return $image[0];
        }
        return $default;
    }


    /**
     * Get excerpt by Post ID
     * @param $postID
     * @return string
     */
    public static function getExcerptByID( $postID ){
        global $post;
        $temp = $post;
        $post = get_post( $postID );
        setup_postdata( $post );
        $excerpt = get_the_excerpt();
        wp_reset_postdata();
        $post = $temp;
        return $excerpt;
    }


    public static function emailContentTypeHTML(){
        return 'text/html';
    }


    /**
     * Send Email Wrapper Function
     * @param $to
     * @param $subject
     * @param $message
     * @param string $headers
     * @param array $attachments
     * @return bool
     */
    public static function sendEmail( $to, $subject, $message, $headers = '', $attachments = array() ){
        global $cwpp;
        if( $cwpp->getEmailType() == CWPP_EMAIL_TYPE_HTML ){
            add_filter( 'wp_mail_content_type', array( 'CWPPStatic', 'emailContentTypeHTML' ) );
        }
        $return = wp_mail( $to, $subject, $message, $headers, $attachments );
        if( $cwpp->getEmailType() == CWPP_EMAIL_TYPE_HTML ){
            remove_filter( 'wp_mail_content_type', array( 'CWPPStatic', 'emailContentTypeHTML' ) );
        }
        return $return;
    }


    public static function form( $formName ){
        self::$currentForm = $formName;
    }

    public static function field( $fieldID ){
        global $cwpp;
        return $cwpp->forms[ self::$currentForm ]->field( $fieldID );
    }

}