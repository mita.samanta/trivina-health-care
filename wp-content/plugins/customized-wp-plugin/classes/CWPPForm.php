<?php

class CWPPForm{


    protected $fields, $deliveries, $formName;

    /**
     * Creates a form. To attach fields drectly,
     * @param array $config An array of configurations & fields.
     */
    public function __construct( $config, $formName ){

        $this->formName     = $formName;
        $this->fields       = [];
        $this->deliveries   = [];

        if( !empty( $config['fields'] ) AND is_array($config['fields']) ){
            $this->addFields( $config['fields'] );
        }

        if( !empty( $config['deliveries'] ) AND is_array( $config['deliveries'] ) ){
            $this->deliveries = $config['deliveries'];
        }

        add_action('init', [$this, 'deliver'], 1000);
    }

    public function field($fieldID){
//        var_dump( $this->fields );
        return $this->fields[ $this->formName . '-' . $fieldID];
    }

    /**
     * Pass a single CWPPField object or array of CWPPField objects, to attach in a form.
     */
    public function addFields($fields){
        if( is_array($fields) ){
            foreach ($fields as $fieldName => $field){
//                $field->name = 'ccwp_form_' . self::class . '[' . $field->name . ']';
                $this->_addField($fieldName, $field);
            }
        }
    }

    protected function _addField($fieldName, CWPPField $field){
        if( isset($this->fields[ $fieldName ]) ){
            throw new Exception('Duplicate Field ID: ' . $field->id);
        }

        $field->prefix = $this->formName . '-';
        $this->fields[ $field->id ] = $field;
    }


    /**
     * @param string $fieldID
     * @param string|int|float $value
     * @throws Exception
     */
    public function __set($fieldID, $value){
        if(!isset( $this->fields[$fieldID] )){
            throw new Exception('Invalid Field');
        }

        $this->fields[$fieldID]->value = $value;
    }

    /**
     * @param string $fieldID
     * @return mixed
     * @throws Exception
     */
    public function __get($fieldID){
        if(!isset( $this->fields[$fieldID] )){
            throw new Exception('Invalid Field');
        }

        return $this->fields[$fieldID]->value;
    }


    /**
     * Validates the form based on fields & value. Returns the validation status of fields.
     * It returns empty array if no validation error occurs. Otherwise will return array of
     * ids, which did not pass the validation function.
     * @return array
     */
    public function validate(){
        $validations = [];
        foreach ($this->fields as $field){
            if(!$field->validate()){
                $validations[] = $field->id;
            }
        }

        return $validations;
    }

    public function register(){

        if( isset( $_REQUEST['ccwp_form_' . self::class ] ) AND !empty( $_REQUEST['ccwp_form_' . self::class ] ) ){

            $formData = $_REQUEST['ccwp_form_' . self::class ];
            if( !is_array( $formData ) ) return false;

            foreach( array_keys( $this->fields ) as $fieldID ){
                if(!isset($formData[$fieldID])) continue;
                $this->{$fieldID} = $formData[$fieldID];
            }

            add_action('init', [ $this, 'deliver' ], 1000);

        }

        return true;

    }

    public function deliver(){
        $validation = $this->validate();

        foreach ( $this->deliveries as $deliveryID => $delivery ){

            do_action('ccwo_form_pre_delivery_' . $deliveryID, $validation );

            $deliveryStatus = call_user_func( $delivery, $this->fields, $validation );

            do_action('ccwo_form_post_delivery_' . $deliveryID, $validation, $deliveryStatus );
        }

    }

}