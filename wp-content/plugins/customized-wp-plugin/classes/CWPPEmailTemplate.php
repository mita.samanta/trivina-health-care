<?php

class CWPPEmailTemplate{

    protected $template;

    /**
     * @param string $template The template is the name of the file without .php extension.
     */
    public function __construct( $template = 'default' ){
        if( $template ) $this->setTemplate( $template );
    }

    /**
     * Sets the Email Template
     * @param string $template
     * @return bool
     */
    public function setTemplate( $template ){
        if( strpos( $template, '/') ) return false;
        $this->template = $template;
        return true;
    }

    /**
     * Send Email using the selected template.
     * @param $to
     * @param $subject
     * @param array $data Data should be in an array.
     * @param array|string $headers
     * @param array|string $attachments
     * @return bool
     */
    public function send( $to, $subject, $data, $headers = '', $attachments = '' ){
        ob_start();
        $templateFile = CWPP_EMAIL_TEMPLATES_DIR . '/' . $this->template . '.php';
        if( file_exists( $templateFile ) ){
            require_once CWPP_EMAIL_TEMPLATES_DIR . '/' . $this->template . '.php';
            $message = ob_get_clean();
            foreach( $data as $key => $val ){
                $message = str_replace( '{{' . $key . '}}', $val, $message );
            }
            if( CWPPStatic::sendEmail( $to, $subject, $message, $headers, $attachments ) ) return true;
        }
        return false;
    }
}