<?php

/**
 * Post Type Wrapper Class
 */

defined( 'CWPP_VERSION' ) or die( 'No script kiddies please!');

class CWPPPostType{

    private $registered = false;

    public
        $labelName = NULL,
        $labelNameSingular = NULL,
        $description = NULL,
        $public = true,
        $showUI = true,
        $hasArchive = false,
        $showInMenu = true,
        $menuPosition = NULL,
        $menuIcon = NULL,
        $excludeFromSearch = false,
        $capabilityType = 'post',
        $mapMetaCap = true,
        $hierarchical = false,
        $rewrite = true,
        $rewriteSlug = NULL,
        $rewriteWithFront = false,
        $rewriteHierarchical = false,
        $queryVar = true,
        $supportsTitle = true,
        $supportsEditor = true,
        $supportsAuthor = true,
        $supportsThumbnail = true,
        $supportsExcerpt = true,
        $supportsCustomFields = false,
        $supportsComments = false,
        $supportsRevisions = true,
        $supportsPageAttributes = false,
        $supportsPostFormats = false
    ;

    public function registerPostType(){
        if( $this->registered ) return;
        $class = get_class( $this );

        if( empty( $this->labelName ) ) $this->labelName = ucfirst( $class );
        if( empty( $this->labelNameSingular ) ) $this->labelNameSingular = $this->labelName;
        if( empty( $this->rewriteSlug ) ) $this->rewriteSlug = $class;

        $labels = array(
            'name' => $this->labelName,
            'singular_name' => $this->labelNameSingular,
            'add_new_item' => __( 'Add New ' . $this->labelNameSingular, CWPP_THEME_TEXT_DOMAIN ),
            'edit_item' => __( 'Edit ' . $this->labelNameSingular, CWPP_THEME_TEXT_DOMAIN ),
            'new_item' => __( 'New ' . $this->labelNameSingular, CWPP_THEME_TEXT_DOMAIN ),
            'view_item' => __( 'View ' . $this->labelNameSingular, CWPP_THEME_TEXT_DOMAIN ),
            'search_items' => __( 'Search ' . $this->labelName, CWPP_THEME_TEXT_DOMAIN ),
            'not_found' => __( 'No ' . $this->labelName . ' found ', CWPP_THEME_TEXT_DOMAIN ),
            'not_found_in_trash' => __( 'No ' . $this->labelName . ' found in Trash', CWPP_THEME_TEXT_DOMAIN ),
            'parent_item_colon' => __( 'Parent ' . $this->labelNameSingular, CWPP_THEME_TEXT_DOMAIN ),
        );

        $rewrite = $this->rewrite ? array(
            'slug' => $this->rewriteSlug,
            'with_front' => $this->rewriteWithFront,
            'hierarchical' => $this->rewriteHierarchical
        ) : false;
        $supports = array();
        if( $this->supportsTitle ) $supports[] = 'title';
        if( $this->supportsEditor ) $supports[] = 'editor';
        if( $this->supportsAuthor ) $supports[] = 'author';
        if( $this->supportsThumbnail ) $supports[] = 'thumbnail';
        if( $this->supportsExcerpt ) $supports[] = 'excerpt';
        if( $this->supportsCustomFields ) $supports[] = 'custom-fields';
        if( $this->supportsComments ) $supports[] = 'comments';
        if( $this->supportsRevisions ) $supports[] = 'revisions';
        if( $this->supportsPageAttributes ) $supports[] = 'page-attributes';
        if( $this->supportsPostFormats ) $supports[] = 'post-formats';
        $args = array(
            'labels'            => $labels,
            'description'       => $this->description,
            'public'            => $this->public,
            'show_ui'           => $this->showUI,
            'has_archive'       => $this->hasArchive,
            'show_in_menu'      => $this->showInMenu,
            'menu_position'     => $this->menuPosition,
            'menu_icon'         => $this->menuIcon,
            'exclude_from_search' => $this->excludeFromSearch,
            'capability_type'   => $this->capabilityType,
            'map_meta_cap'      => $this->mapMetaCap,
            'hierarchical'      => $this->hierarchical,
            'query_var'         => $this->queryVar,
            'rewrite'           => $rewrite,
            'supports'          => $supports,
        );
        register_post_type( $class, $args );
        $this->registered = true;
    }

}