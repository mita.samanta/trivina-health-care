<?php

// TODO: Should support radio & select fields in groups. Also validate in groups.
// TODO: Multiple selection of select fields in validation.

class CWPPField{

    protected $name;

    public $prefix = 'cwppfield-', $value, $validator, $type, $attributes, $options;

    const TYPE_HIDDEN = 'hidden';
    const TYPE_TEXT = 'text';
    const TYPE_EMAIL = 'email';
    const TYPE_PASSWORD = 'password';
    const TYPE_TEXTAREA = 'textarea';
    const TYPE_CHECKBOX = 'checkbox';
    const TYPE_CHECKBOX_GROUP = 'checkbox-group';
    const TYPE_RADIO = 'radio';
    const TYPE_SELECT = 'select';


    /**
     * CWPPField constructor.
     * @param string $name
     * @param string $hint
     * @param string $type
     * @param string $id
     * @param string $validator
     */
    public function __construct($name, $hint, $type = 'text', $id = '', $validator = '', $attributes = [] ){

        // Initialization ================
        $this->type         = $type;
        $this->validator    = $validator;
        $this->attributes   = array();


        // Convert data array as separate data attributes ======
        if( isset( $attributes['data'] ) AND is_array( $attributes['data'] ) ){
            foreach( $attributes['data'] as $key => $val ){
                $attributes['data-' . $key] = $val;
            }
            unset( $attributes['data'] );
        }


        // Override Parametrized attributes into array, this also prioritizes params than attribute array =====
        $attributes = array_merge( $attributes, [
            'name' => $name,
            'id' => $id,
            'placeholder' => $hint,
        ]);

        // If id is empty, use field name as id =======
        if( empty( $attributes['id'] ) ) $attributes['id'] = $attributes['name'];


        // Set Attributes into field ======
        foreach ($attributes as $key => $val ){
            $this->{$key} = $val;
        }

    }



    public function data($name, $value = NULL){

        if( $value === NULL ){
            return $this->{'data-' . $name};
        }

        if( $value === false ) $value = 'false';
        if( $value === true ) $value = 'true';

        $this->attrib('data-' . $name, $value);

    }

    public function __set($name, $value){

        if( $name == 'options' ){
            if( is_array( $value ) ){
                $this->options = $value;
            }
        }else{
            $this->attrib($name, $value);
        }
    }

    public function __get($name){
        if( isset( $this->attributes[ $name ] ) ){
            if( 'name' == $name OR 'id' == $name ){
                return $this->prefix . $this->attributes[ $name ];
            }
            return $this->attributes[ $name ];
        }
        return '';
    }

    public function __isset($name){
        return isset( $this->attributes[ $name ] );
    }

    public function attrib($name, $value = '', $replace = true){
        if( $replace ) $this->attributes[ $name ] = $value;
        else $this->attributes[ $name ] .= ' ' . $value;
        return $this;
    }


    public function attribs( $attributes, $replace = true ){

        foreach ($attributes as $key => $val){
            $this->attrib( $key, $val, $replace );
        }

        return $this;
    }

    public function _($name){
        echo $this->{$name};
    }

    /**
     * Validates the field based on the validator function.
     * @return bool|mixed
     */
    public function validate(){
        if( !empty($this->validator) ){
            return call_user_func( $this->validator, $this->value );
        }

        return true;
    }


    /**
     * Generates the HTML for the field
     */
    public function ui($groupIndex = ''){

        $validatorClass = empty( $this->validator ) ? '' : ' cwppfield-validate-' . $this->validator;
        $fieldClass = $this->class . ' cwppfield ' . $this->name . $validatorClass;
        $fieldValue = htmlentities( $this->value );

        $attributes = '';
        foreach( array_keys( $this->attributes ) as $attribute ){
            $attribute  = addslashes( $attribute );
            $value      = htmlentities( $this->{$attribute} );
            $attributes .= ' ' . $attribute . '="' . $value . '"';
        }

        $attributes .= ' name=' . htmlentities($this->prefix . $this->name);

        switch ($this->type){

            case self::TYPE_TEXTAREA:
                echo "<textarea class='$fieldClass' $attributes>$fieldValue</textarea>";
                break;


            case self::TYPE_SELECT:
                if( isset( $attributes['value'] ) ) unset($attributes['value']);
                echo "<select class='$fieldClass' $attributes>";
                foreach( $this->options as $key => $val ){
                    $key = htmlentities( $key );
                    $val = htmlentities( $val );
                    $selected = $fieldValue === $key ? 'selected="selected"' : '';
                    echo "<option value='$key' $selected>$val</option>";
                }
                echo "</select>";
                break;

            case self::TYPE_CHECKBOX:
                echo "<input  class='$fieldClass' value='$fieldValue' type='checkbox' $attributes />";
                break;

            case self::TYPE_RADIO:
                echo "<input  class='$fieldClass' value='$this->value' type='radio' $attributes />";
                break;


	        case self::TYPE_CHECKBOX_GROUP:

		        if( isset( $attributes['value'] ) ) unset($attributes['value']);
//		        echo "<select class='$fieldClass' $attributes>";
//		        foreach( $this->options as $key => $val ){
//			        $key = htmlentities( $key );
//			        $val = htmlentities( $val );
//			        $selected = $fieldValue === $key ? 'selected="selected"' : '';
//			        echo "<option value='$key' $selected>$val</option>";
//		        }
//		        echo "</select>";

		        echo "<input  class='$fieldClass' value='$fieldValue' type='checkbox' $attributes />";
		        break;

//            case self::TYPE_HIDDEN:
//            case self::TYPE_TEXT:
//            case self::TYPE_EMAIL:
//            case self::TYPE_PASSWORD:
            default:
                echo "<input  class='$fieldClass' value='$fieldValue' type='$this->type' $attributes />";
                break;
        }

    }

}
