Customized WP Plugin
=====================

A meta plugin that abstracts the functionality of WordPress in simpler manner.
This plugin originates from "Customized WP" Meta Template by UFLIX DESIGN. But, 
it has no direct relationship with the theme. Both of them may reside together.
Though, this is not recomended.

The approach of this plugin is to decouple theme functions and the functionality
of Customized WP. Also, with minor adjustments, one can upgrade existing
**Customized WP** based theme to **Customized WP Pluhin**
