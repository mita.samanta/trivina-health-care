<?php

/**
 * Plugin Name: Customized WP Plugin
 * Description: A meta plugin that abstracts the functionality of WordPress in simpler manner. This plugin originates from "Customized WP" Meta Template by UFLIX DESIGN.
 * Author: UFLIX DESIGN
 * Author URI: http://uflixdesign.com
 * Version: 0.0.1
 *
 * Note for Developers: This plugin is not compatible with "Customized WP" Meta Theme.
 * As, we may add or dropout many functionality to achieve better, faster and easier
 * solution for developers.
 */

defined( 'ABSPATH' ) or die( 'No script kiddies please!');


// If this plugin already loaded elsewhere then do not load this anymore.
if( defined( 'CWPP_VERSION' ) ) return;


require_once( dirname( __FILE__ ) . '/constants.php');

require_once( CWPP_DIR . '/classes/CWPPStatic.php' );
require_once( CWPP_DIR . '/classes/CWPPPostType.php' );
require_once( CWPP_DIR . '/classes/CWPPTaxonomy.php' );
require_once( CWPP_DIR . '/classes/CWPPEmailTemplate.php' );
require_once( CWPP_DIR . '/classes/CWPPField.php' );
require_once( CWPP_DIR . '/classes/CWPPForm.php' );
require_once( CWPP_DIR . '/classes/CWPPAjaxForm.php' );
require_once( CWPP_DIR . '/classes/CustomizedWPPlugin.php' );


/**
 * Instantiate CustomizedWPPlugin class.
 * $cwpp is a global object variable of same class.
 */

global $cwpp;
$cwpp = new CustomizedWPPlugin( CWPP_CONFIG_FILE );