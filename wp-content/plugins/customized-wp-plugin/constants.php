<?php

defined( 'ABSPATH' ) or die( 'No script kiddies please!');


/**
 * Lets CWPP be the prefix for or constants and any strings or functions
 * for Customized WP Plugin.
 */


define( 'CWPP_VERSION', '0.0.1' );


define( 'CWPP_DIR',         dirname(__FILE__) );
define( 'CWPP_URI',         plugins_url('', __FILE__) );
define( 'CWPP_THEME_DIR',   get_template_directory() );
define( 'CWPP_THEME_URI',   get_template_directory_uri() );
define( 'CWPP_AJAX_URI',    get_bloginfo('url') . '/?cwpp_ajax' );


/** Theme Mods */
define( 'CWPP_THEME_MOD_DIR',       CWPP_THEME_DIR . '/cwpp' );
define( 'CWPP_THEME_MOD_URI',       CWPP_THEME_URI . '/cwpp' );
define( 'CWPP_CONFIG_FILE',         CWPP_THEME_MOD_DIR . '/config.php' );
define( 'CWPP_POST_TYPES_DIR',      CWPP_THEME_MOD_DIR . '/post-types' );
define( 'CWPP_TAXONOMIES_DIR',      CWPP_THEME_MOD_DIR . '/taxonomies' );
define( 'CWPP_WIDGETS_DIR',         CWPP_THEME_MOD_DIR . '/widgets' );
define( 'CWPP_FORMS_DIR',           CWPP_THEME_MOD_DIR . '/forms' );
define( 'CWPP_EMAIL_TEMPLATES_DIR', CWPP_THEME_MOD_DIR . '/email-templates' );


/** Config Elements */
define( 'CWPP_CFG_STYLES',          'styles' );
define( 'CWPP_CFG_SCRIPTS',         'scripts' );
define( 'CWPP_CFG_IMAGE_SIZES',     'image-sizes' );
define( 'CWPP_CFG_SIDEBARS',        'sidebars' );
define( 'CWPP_CFG_MENUS',           'menus' );
define( 'CWPP_CFG_THEME_SUPPORTS',  'theme-supports' );
define( 'CWPP_CFG_EMAIL',           'email' );
define( 'CWPP_CFG_EMAIL_METHOD',    'method' );
define( 'CWPP_CFG_EMAIL_SMTP_HOST',      'host' );
define( 'CWPP_CFG_EMAIL_SMTP_PORT',      'port' );
define( 'CWPP_CFG_EMAIL_SMTP_AUTH',      'auth' );
define( 'CWPP_CFG_EMAIL_SMTP_AUTH_USER', 'user' );
define( 'CWPP_CFG_EMAIL_SMTP_AUTH_PASS', 'pass' );
define( 'CWPP_CFG_EMAIL_SMTP_SECURE',    'secure' );


define( 'CWPP_EMAIL',                   'email' );
define( 'CWPP_EMAIL_TYPE_PLAIN',        'plain' );
define( 'CWPP_EMAIL_TYPE_HTML',         'html' );
define( 'CWPP_EMAIL_METHOD_DEFAULT',    'mail' );
define( 'CWPP_EMAIL_METHOD_SMTP',       'smtp' );
define( 'CWPP_EMAIL_SECURE_NONE',       false );
define( 'CWPP_EMAIL_SECURE_TLS',        'tls' );
define( 'CWPP_EMAIL_SECURE_SSL',        'ssl' );


define( 'CWPP_AJAX_RESPONSE_RAW',       'raw' );
define( 'CWPP_AJAX_RESPONSE_JSON',      'json' );
define( 'CWPP_AJAX_RESPONSE_SCRIPT',    'script' );


$CWPPTheme = wp_get_theme();
define( 'CWPP_THEME_TEXT_DOMAIN',   $CWPPTheme->get('TextDomain') );
define( 'CWPP_THEME_VERSION',       $CWPPTheme->get('Version') );