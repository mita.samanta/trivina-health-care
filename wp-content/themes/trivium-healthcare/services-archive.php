<?php 
/*
* Template Name: Service-Archive Page
*/
get_header();
?>

<div class="shadow"></div>
	<div class="post-wrapper-top clearfix">
		<div class="container">
			<div class="col-lg-12">
				<h2><?php the_title();?></h2>
                <!-- <ul class="breadcrumb pull-right">
                    <li><a href="index-2.html">Home</a></li>
                    <li>Services</li>
                </ul> -->
			</div>
		</div>
	</div><!-- end post-wrapper-top -->

	<div class="white-wrapper">
    	<div class="container">
        	<div class="general-row">
            	<div class="general-title text-center">
                	<h3><?php the_field('service_title');?></h3>
                    <p class="lead"><?php the_field('service_text');?></p>
                </div><br>
             
                <div class="custom-services">

                     <?php
                            $args = array(
                            'post_type' => 'post',
                            'orderby' => 'date' ,
                            'order' => 'DESC' ,
                            
                            'cat'         => 'Services',
                            'paged' => get_query_var('paged'),
                            'post_parent' => $parent
                            );
                            $the_query = new WP_Query($args); ?>
                            <?php if ( $the_query->have_posts() ) : ?>
                            <?php while ( $the_query->have_posts() ) : $the_query->the_post();
                            // var_dump($post); exit();
                            // $categories = get_the_terms($post->ID,'event_categories'); ?>

                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 ">
						<div class="ch-item single-layer">	
							<div class="ch-info-wrap">
								<div class="ch-info">
									<div class="depart-ch ch-info-front">
                                    <?php the_field('service_icon');?>
                                        <h3><?php the_title(); ?></h3>
                                        <p><?php the_field('short_details'); ?></p>
                                    </div>
									<div class="depart-ch ch-info-back">
                                    
                                        <?php the_field('service_icon');?>
                                        <h3><?php the_title(); ?></h3>
                                        <p><?php the_field('short_details'); ?></p>
                                        <a class="readmore" href="<?php the_permalink() ?>" title="">Read More </a>
                                    </div>
								</div><!-- end ch-info -->
							</div><!-- end ch-info-wrap -->
						</div><!-- end ch-item -->
                    </div><!-- end col-sm-3 -->
                    <?php endwhile; ?>
                                <?php wp_reset_postdata(); ?>
                            <?php else : ?>
                                <p><?php esc_html_e( 'Sorry, no service found.' ); ?></p>
                            <?php endif; ?>
                    
                    
                </div><!-- end custom-services -->
                
                <div class="clearfix"></div>
                
				<div class="calloutbox">
					<div class="col-lg-9 col-md-12 col-sm-12">
                        <h2><?php the_field('appointment_text');?></h2>
                        <p><?php the_field('appointment_sub-text');?></p>
                    </div>
                    <div class="col-lg-3 col-md-12 col-sm-12">
                        <a class="btn pull-right btn-dark btn-lg margin-top" href="<?php the_field('contact_link','options');?>"><?php the_field('appointment_button_text','options');?><i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                    </div>
                </div><!-- end messagebox --> 
 
            </div><!-- end row -->
        </div><!-- end container -->
    </div><!-- end white-wrapper -->

   
	


   <?php get_footer();?>