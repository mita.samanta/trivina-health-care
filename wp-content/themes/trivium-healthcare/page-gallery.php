<?php 
/*
* Template Name: Gallery Page
*/
get_header();
?>
	<div class="shadow"></div>

	<div class="post-wrapper-top clearfix">
		<div class="container">
			<div class="col-lg-12">
				<h2><?php the_title();?></h2>
                <!-- <ul class="breadcrumb pull-right">
                    <li><a href="index-2.html">Home</a></li>
                    <li>Gallery One</li>
                </ul> -->
			</div>
		</div>
	</div><!-- end post-wrapper-top -->

    


    <div class="white-wrapper gal-view">
    	<div class="container">
    		<div id="content" class="col-lg-8 col-md-12 col-sm-8 col-xs-12">
                <div class="big-title clearfix">
                    <h3 >Our Gallery</h3>
                    <h3 class="gal-view" ><?php if( function_exists('photo_gallery') ) { photo_gallery(3); } ?></h3>
                   

                  
                   
                </div><!-- end big title -->	
                
                

                
                
				
                
            </div><!-- end content -->
    
    		<div id="sidebar" class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
            	<div class="widget">
                	<div class="title">
                    	<h2>ALL DEPARTMENT</h2>
                    </div><!-- end title -->
					<?php $terms = get_terms( array(
                                    'post_type' => 'doctor',
                                    'taxonomy' => 'doctor_categories',
                                    'has_archive' => true,
                                    'public' => true,
                                    'hierarchical' => false,
                                    'hide_empty' => false,
                                    ) );
                                
                                        foreach ($terms as $term):
                                            // var_dump($term); exit();
                                            $slug = $term->slug;
                                    
                                    ?>
                    <div class="row">
                        <div class="col-lg-3 col-md-6">
                                <ul class="recent_posts_widget">
                                
                                <li>
                                
                                <a href="<?php echo get_term_link( $term->term_id ) ?>">
                                
                                <img src="<?php the_field('iconic_image',$term); ?>" alt="" />
                                
                                </a>
                                
                                
                                </li>
                                
                            </ul><!-- recent posts --> 
                        </div>
                        <div class="col-6">
                        <p class="head-name"><?php echo $term->name; ?></p>
                        <a class="readmore_widget" href="<?php echo get_term_link( $term->term_id ) ?>"><p>Click Here</p></a>
                        </div>
                    </div>
                    <?php endforeach; ?>  
                </div><!-- end widget -->
                
            	
                
            	
            </div><!-- end sidebar -->
    	</div><!-- end container -->
    </div><!-- end white-wrapper -->





    <div class="grey-wrapper gallery-doc">
    	<div class="container">
        	<div class="general_row">
            	<div class="big-title clearfix">
                	<h3>OUR DOCTORS</h3>
                </div><!-- end big title -->
                
                <div class="carousel_widget">
					<div class="team_widget below doc-view">
                    	<ul id="owl-blog" class="owl-carousel">
                                <?php
                                $args = array(
                                'post_type' => 'doctor',
                                // 'taxonomy' => 'property_categories',
                                // 'taxonomy' => 'property_locations',
                                // 'taxonomy' => 'property_rooms',
                                // 'taxonomy' => 'property_types',
                                'hide_empty' => true,
                                );
                                $the_query = new WP_Query($args); ?>
                                <?php if ( $the_query->have_posts() ) : ?>
                                <?php while ( $the_query->have_posts() ) : $the_query->the_post(); 
                                    $terms = get_the_terms($post, 'doctor_categories');
                                    
                                ?>
                        	<li>
                            	<img src="<?php the_field('doctor_image');?>" class="img-responsive img-circle alignleft" alt=""> 
                                  <h3><span><?php the_title();?></span></h3>
                                                <?php
                                    // var_dump($terms);exit();
                                    foreach($terms as $term):
                                        
                                    ?>
                                <h4><?php echo $term->name; ?></h4>
                                <?php endforeach; ?>
                                <p><?php the_field('doctor_degree');?></p>
                                
                            </li>
                        	<?php endwhile; ?>
                                <?php wp_reset_postdata(); ?>
                            <?php else : ?>
                                <p><?php esc_html_e( 'Sorry, no service found.' ); ?></p>
                            <?php endif; ?>
                        	                 
                    	</ul>
                    </div><!-- end team_widget -->  
            	</div><!-- end carousel_widget -->     
            </div><!-- end general_row -->

			
        </div><!-- end container -->
    </div><!-- end white-wrapper -->
    




    








	
    
   <?php get_footer();?>