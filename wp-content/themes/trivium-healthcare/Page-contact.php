<?php 
/*
* Template Name: Contact Page
*/
get_header();
?>


	<div class="shadow"></div>

	<div class="post-wrapper-top clearfix">
		<div class="container">
			<div class="col-lg-12">
				<h2>Contact</h2>
                <!-- <ul class="breadcrumb pull-right">
                    <li><a href="index-2.html">Home</a></li>
                    <li>Contact</li>
                </ul> -->
			</div>
		</div>
	</div><!-- end post-wrapper-top -->

    <div id="map"></div>
    
	<div class="clearfix"></div>
        
	<div class="white-wrapper">
    	<div class="container">
			<div class="row ">
				<div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
					<div class="big-title clearfix">
						<h3>Write Us</h3>
					</div><!-- end big title -->
                    <div class="contact_form">
                    <div id="message"></div>
                        <form id="" action="#" method="post" class="cwpp-ajax row online_form_builder_big">
                            <input type="hidden" name="action" value="contact">
                            <div class="col-md-6">
                                <label for="ufxd-contact-name">Your Name </label>
                                <input type="text" class="form-control" id="ufxd-contact-name" name="ufxd-contact-name" placeholder="" required>
                            </div>
                            <div class="col-md-6">
                                <label for="ufxd-contact-email">Email Address </label>
                                <input type="text" class="form-control" id="ufxd-contact-email" name="ufxd-contact-email" required>
                            </div>
                            <div class="col-md-12">
                                <label for="ufxd-contact-subject">Subject</label>
                                <input type="text" class="form-control" id="ufxd-contact-subject" name="ufxd-contact-subject">
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-12">
                            <label for="ufxd-contact-message">Your Message</label>
                            <textarea class="form-control" id="ufxd-contact-message" name="ufxd-contact-message"></textarea>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-12">
                            <button type="submit"  class="btn btn-lg btn-primary pull-right">SEND NOW</button>   
                            </div> 
                            <div class="output"></div>  
                        </form>
                     </div>
				</div>
				<div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
					<div class="big-title clearfix">
						<h3>Contact Info</h3>
					</div><!-- end big title -->
                    
                    <p>Have any Queries? Let us know. We will clear it for you at the best.</p>
                    
                    <ul class="contact_details_1">
                    	<li><i class="fa fa-map-marker"></i> <span>Address:</span> <?php the_field('address','options');?></li>
                    	<li><i class="fa fa-mobile-phone"></i> <span>Phone:</span> <?php the_field('phone','options');?> 
						</li>
                    	<li>
                        <i class="fa fa-envelope-o"></i> <span>Email:</span> <?php the_field('email','options');?>
						</li>
                    </ul>
                    
				</div> 
			</div>
        </div><!-- end container -->
    </div><!-- end white-wrapper -->
    
    <?php get_footer();?>