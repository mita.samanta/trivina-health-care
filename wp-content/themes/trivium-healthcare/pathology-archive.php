<?php 
/*
* Template Name: Pathology-Archive Page
*/
get_header();
?>
	<div class="shadow"></div>

	<div class="post-wrapper-top clearfix">
		<div class="container">
			<div class="col-lg-12">
				<h2><?php the_title();?></h2>
                <!-- <ul class="breadcrumb pull-right">
                    <li><a href="index-2.html">Home</a></li>
                    <li>Hospital Staffs</li>
                </ul> -->
			</div>
		</div>
	</div><!-- end post-wrapper-top -->

	<div class="white-wrapper">
    	<div class="container">
        	<div class="general_row">
            
                <div class="general-title text-center">
                	<h3><?php the_field('pathology_title');?></h3>
                    <p class="lead"><?php the_field('pathology_text');?></p>
                </div>
                
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 doc-view">
					<div class="team_widget doc">
                    	<ul>

                        <?php
                                $args = array(
                                'post_type' => 'pathology',
                                // 'taxonomy' => 'property_categories',
                                // 'taxonomy' => 'property_locations',
                                // 'taxonomy' => 'property_rooms',
                                // 'taxonomy' => 'property_types',
                                'hide_empty' => true,
                                );
                                $the_query = new WP_Query($args); ?>
                                <?php if ( $the_query->have_posts() ) : ?>
                                <?php while ( $the_query->have_posts() ) : $the_query->the_post(); 
                                    // $terms = get_the_terms($post, 'doctor_categories');
                                    
                                ?>
                        	<li class="path">
                                <div class="row">
                                    <div class="col-12 path-arch">
                                        
                                            <ul class="sliders">
                                                <?php $collects= get_field('test_gallery');
                                                foreach ($collects as $collect ) :
                                                    
                                                    ?>
                                                
                                                <li >
                                                    <a class="plan lightgallery" href="<?php echo $collect['view']['url']?>">
                                                        <img src="<?php echo $collect['view']['url'] ?>" alt="" class=" depart-single img-responsive alignleft">
                                                    </a>
                                                    
                                                </li>
                                                <?php endforeach;?>
                                            </ul><!-- end slides -->
                                            
                                        
                                    </div>   
                                    <div class="col-lg-12 col-md-12 col-sm-12 doctor-info path-info">
                                    <h3><span><?php the_title();?></span></h3>
                                    <p><?php the_field('test_description');?> </p>
                                    <?php 
                                $value = get_field('test-price');
                                if(!empty ($value)):
                                    ?>
                                <span><?php the_field('test_price_title');?> <h4><?php the_field('test-price'); ?></h4></span>
                                <?php endif;?>
                                <?php 
                                $value = get_field('report');
                                if(!empty ($value)):
                                    ?>
                                <span><?php the_field('report_title');?> <h4><?php the_field('report');?></h4></span>
                                <?php endif;?>
                                <?php 
                                $value = get_field('day');
                                if(!empty ($value)):
                                    ?>
                                <span><?php the_field('day_title');?><h4><?php the_field('day'); ?></h4></span>
                                <?php endif;?>
                                <?php 
                                $value = get_field('time');
                                if(!empty ($value)):
                                    ?>
                                <span><?php the_field('time_title');?>  <h4><?php the_field('time'); ?></h4></span>
                                <?php endif;?>
                                
                                <button class="btn pathlogy-button" data-toggle="modal" id="clickme" data-target="#myModal-<?php the_ID(); ?>">Make an Appointment <i class="fa fa-arrow-right" aria-hidden="true"></i></button>
                                    </div>
                                </div>
                            	
                                  
                            </li>
                            <?php endwhile; ?>
                                <?php wp_reset_postdata(); ?>
                            <?php else : ?>
                                <p><?php esc_html_e( 'Sorry, no service found.' ); ?></p>
                            <?php endif; ?>            
                    	</ul>
                    </div><!-- end team_widget -->   
                </div><!-- end col -->

                
                
            </div><!-- end row -->
        </div><!-- end container -->
    </div><!-- end white-wrapper -->

	<div class="grey-wrapper">
    	<div class="container">
        	<div class="general_row">
            	<div class="big-title clearfix">
                	<h3>Our Services</h3>
                </div><!-- end big title -->
                
                <div class="carousel_widget">
					<div class="team_widget">
                    	<ul id="owl-blog" class="owl-carousel">
                        <?php
                            $args = array(
                            'post_type' => 'post',
                            'orderby' => 'date' ,
                            'order' => 'DESC' ,
                            
                            'cat'         => 'Services',
                            'paged' => get_query_var('paged'),
                            'post_parent' => $parent
                            );
                            $the_query = new WP_Query($args); ?>
                            <?php if ( $the_query->have_posts() ) : ?>
                            <?php while ( $the_query->have_posts() ) : $the_query->the_post();
                            // var_dump($post); exit();
                            // $categories = get_the_terms($post->ID,'event_categories'); ?>
                        	<li class="service-doc ">
                                <?php $collects= get_field('service_image');
                                        $collect11=array_shift($collects); 
                                            
                                            ?>
                                <a class="service-img" href="<?php the_permalink() ?>">
                                    <img src="<?php echo $collect11 ['image-view']['url'] ?>" > 
                                </a>
                                <div class="desc">
                              	<h3><span><?php the_title(); ?></span></h3>
                                
                                <p><?php the_field('short_details'); ?></p>
                                </div>
                                
                            </li>
                            <?php endwhile; ?>
                                <?php wp_reset_postdata(); ?>
                            <?php else : ?>
                                <p><?php esc_html_e( 'Sorry, no service found.' ); ?></p>
                            <?php endif; ?>               
                    	</ul>
                    </div><!-- end team_widget -->  
            	</div><!-- end carousel_widget -->     
            </div><!-- end general_row -->
        </div><!-- end container -->
    </div><!-- end white-wrapper -->
    


                               
    <?php
                                $args = array(
                                'post_type' => 'pathology',
                                // 'taxonomy' => 'property_categories',
                                // 'taxonomy' => 'property_locations',
                                // 'taxonomy' => 'property_rooms',
                                // 'taxonomy' => 'property_types',
                                'hide_empty' => true,
                                );
                                $the_query = new WP_Query($args); ?>
                                <?php if ( $the_query->have_posts() ) : ?>
                                <?php while ( $the_query->have_posts() ) : $the_query->the_post(); 
                                   
                                    
                                ?>



<!-- Modal -->
                            <div class="modal fade" id="myModal-<?php the_ID(); ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title" id="myModalLabel"></h4>

                                </div>
                                <div class="modal-body">
                                <h3 ><?php the_title(); ?></h3>

                                <?php 
                                $value = get_field('day');
                                if(!empty ($value)):
                                    ?>
                                <span><?php the_field('day_title');?><h4><?php the_field('day'); ?></h4></span>
                                <?php endif;?>
                                <?php 
                                $value = get_field('time');
                                if(!empty ($value)):
                                    ?>
                                <span><?php the_field('time_title');?>  <h4><?php the_field('time'); ?></h4></span>
                                <?php endif;?>
                                </div>


                                <form id="" action="#" method="post" class="cwpp-ajax row online_form_builder_big">
                                    <input type="hidden" name="action" value="pathology">
                                    <input type="hidden" name="ufxd-pathology-name" id="ufxd-pathology-name" value="<?php the_title(); ?>">
                                    <div class="col-md-12">
                                        <label for="ufxd-path-name">Patient Name </label>
                                        <input type="text" class="form-control" id="ufxd-path-name" name="ufxd-path-name" placeholder="" required>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="ufxd-path-time">Time </label>
                                        <input type="time" class="form-control" id="ufxd-path-time" name="ufxd-path-time" required>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="ufxd-path-day">Day </label>
                                        <input type="date" class="form-control" id="ufxd-path-day" name="ufxd-path-day" required>
                                    </div>
                                    <div class="col-md-6">
                                    
                                        <label for="ufxd-path-email">Email Address </label>
                                        <input type="text" class="form-control" id="ufxd-path-email" name="ufxd-path-email">
                                    </div>
                                    <div class="col-md-6">
                                        <label for="ufxd-path-phone">Phone Number </label>
                                        <input type="text" class="form-control" id="ufxd-path-phone" name="ufxd-path-phone" required>
                                    </div>
                                    <div class="col-md-12">
                                        <label for="ufxd-path-message">Subject</label>
                                        <input type="text" class="form-control" id="ufxd-path-message" name="ufxd-path-message">
                                    </div>
                                    <!-- <div class="clearfix"></div>
                                    <div class="col-md-12">
                                    <label for="comments">Your Message</label>
                                    <textarea class="form-control" id="comments"></textarea>
                                    </div> -->
                                    <div class="clearfix"></div>
                                    <div class="col-md-12">
                                    <button type="submit"  class="btn btn-lg btn-primary pull-right">SEND NOW</button>   
                                    <div class="output"></div>
                                    </div>   
                                </form>

                                <!-- <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    <button type="button" class="btn btn-primary">Save changes</button>
                                </div> -->
                                </div><!-- /.modal-content -->
                            </div><!-- /.modal-dialog -->
                            </div><!-- /.modal -->
                            <?php endwhile; ?>
                                <?php wp_reset_postdata(); ?>
                            <?php else : ?>
                                <p><?php esc_html_e( 'Sorry, no service found.' ); ?></p>
                            <?php endif; ?>                        


                            

    <?php get_footer();?>