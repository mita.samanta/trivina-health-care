<footer class="footer">
    	<div class="container">
        	<div class="row">
                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                	<div class="widget footer-logo">
                        <a href="<?php the_field('logo_link','options');?>"><img src="<?php the_field('logo_footer','options');?>" alt="JollyMedic" class="flogo img-responsive"></a>
                    	
						<p><?php the_field('footer_about','options');?></p>
						
                        <div class="social-icons">
                        <?php 
                        $value = get_field('facebook' , 'options');
                        if(!empty ($value)):
                            ?>
						<span class="facebook"><a data-toggle="tooltip" data-placement="bottom" title="Facebook" href="<?php the_field('facebook','options');?>"><i class="fa fa-facebook"></i></a></span>
                        <?php endif;?>

                        <?php 
                        $value = get_field('google-plus' , 'options');
                        if(!empty ($value)):
                            ?>
						<span class="google-plus"><a data-toggle="tooltip" data-placement="bottom" title="Google Plus" href="<?php the_field('google-plus','options');?>"><i class="fa fa-google-plus"></i></a></span>
                        <?php endif;?>

                        <?php 
                        $value = get_field('twitter' , 'options');
                        if(!empty ($value)):
                            ?>
						<span class="twitter"><a data-toggle="tooltip" data-placement="bottom" title="Twitter" href="<?php the_field('twitter','options');?>"><i class="fa fa-twitter"></i></a></span>
                        <?php endif;?>

                        <?php 
                        $value = get_field('linkedin' , 'options');
                        if(!empty ($value)):
                            ?>
						<span class="linkedin"><a data-toggle="tooltip" data-placement="bottom" title="Linkedin" href="<?php the_field('linkedin','options');?>"><i class="fa fa-linkedin"></i></a></span>
                        <?php endif;?>

                        <?php 
                        $value = get_field('skype' , 'options');
                        if(!empty ($value)):
                            ?>
						<span class="skype"><a data-toggle="tooltip" data-placement="bottom" title="Skype" href="<?php the_field('skype','options');?>"><i class="fa fa-skype"></i></a></span>
                        <?php endif;?>
                        </div><!-- end social icons -->
					</div><!-- end widget -->
                </div><!-- end col -->
                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                	<div class="widget">
                        <div class="title">
                           <h3>OUR SERVICES</h3>
                        </div><!-- end title -->
                        <ul class="twitter_feed">
                            <?php
                            $args = array(
                            'post_type' => 'post',
                            'orderby' => 'date' ,
                            'order' => 'DESC' ,
                            
                            'cat'         => 'Services',
                            'paged' => get_query_var('paged'),
                            'post_parent' => $parent
                            );
                            $the_query = new WP_Query($args); ?>
                            <?php if ( $the_query->have_posts() ) : ?>
                            <?php while ( $the_query->have_posts() ) : $the_query->the_post();
                            // var_dump($post); exit();
                            // $categories = get_the_terms($post->ID,'event_categories'); ?>
                        
                            <li>
                            <a href="<?php the_permalink() ?>"><h3><?php the_title(); ?></h3></a>
                            </li>
                            <?php endwhile; ?>
                                <?php wp_reset_postdata(); ?>
                            <?php else : ?>
                                <p><?php esc_html_e( 'Sorry, no service found.' ); ?></p>
                            <?php endif; ?>
                        </ul><!-- end twiteer_feed --> 
                    </div><!-- end widget -->
                </div><!-- end col -->
                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                	<div class="widget depart-menu">
                        <div class="title">
                           <h3>OUR DEPARTMENTS</h3>
                        </div><!-- end title -->
                        <?php
                                wp_nav_menu([
                                    'theme_location' => 'services',
                                    'container' => 'ul',
                                    'menu_class' => 'recent_posts_widget',
                                    'depth' => 2,
                                    'fallback_cb'     => 'WP_Bootstrap_Navwalker::fallback',
                                    'walker'          => new WP_Bootstrap_Navwalker(),
                                ]);
                            ?>
                        <!-- <ul class="recent_posts_widget">

                        </ul> recent posts -->   
                    </div><!-- end widget -->
                </div><!-- end col -->
                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                	<div class="widget">
                        <div class="title">
                           <h3>Contact Info</h3>
                        </div><!-- end title -->
                        <div class="contact_widget">
                        	<ul>
                            	<li><i class="fa fa-map-marker"></i> <?php the_field('address','options');?></li>
								<li><i class="fa fa-envelope-o"></i> <a href="mailto:support@yoursite.com" title=""><?php the_field('email','options');?></a></li>
                                <li><i class="fa fa-phone"></i> Phone: <?php the_field('phone','options');?></li>
                            </ul>
                            
                        </div><!-- end contact_widget -->
                    </div><!-- end widget -->
                </div><!-- end col -->    
            </div><!-- end row -->
        </div><!-- end container -->
    </footer><!-- end footer -->
    
    <div class="copyright">
    	<div class="container">
        	<div class="row">
                <div class="col-lg-6 col-md-12 col-sm-12">
                    <div class="copyright-text">
                        <p>Copyright &copy;<script>document.write(new Date().getFullYear());</script> - <?php echo bloginfo('name'); ?> </p>
                    </div><!-- end copyright-text -->
                </div><!-- end widget -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                    <div class="designer-text">
                    <a href="http://uflixdesign.com">Designed by UFLIX DESIGN</a>
                            
                    </div>
                </div><!-- end large-7 --> 
            </div><!-- end row -->
        </div><!-- end container -->
    </div><!-- end copyrights -->
    
	<div class="dmtop">Scroll to Top<i class="fas fa-chevron-up"></i></div>
    
    <!-- Main Scripts-->
    

	<!-- CALENDAR WIDGET  -->
	

    <!-- SLIDER REVOLUTION 4.x SCRIPTS  -->
    
	
    <?php wp_footer(); ?>
</body>

<!-- Mirrored from jollythemes.com/html/jollymedic/index1.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 22 Jun 2020 12:34:51 GMT -->
</html>
