<?php 
/*
* Template Name: Home Page
*/
get_header();
?>


<div class="shadow"></div>

   <div class="slider-wrapper">
        <div class="tp-banner-container">
            <div class="tp-banner" >
                <ul>
                    <?php $numbers= get_field('home_slider');
                    foreach($numbers as $number):

                    ?>
					<li data-transition="fade" data-slotamount="7" data-masterspeed="1500" >
                        <img src="<?php echo $number['slider_image']['url'];?>"  alt="slidebg1"  data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">
                        <div class="tp-caption big_title_slider customin customout start"
                            data-x="center" data-hoffset="210"
                            data-y="170"
                            data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                            data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                            data-speed="1000"
                            data-start="500"
                            data-easing="Back.easeInOut"
                            data-endspeed="300"><span><?php echo $number['slider_heading'];?></span><br>
							
                        </div>
                        
                        <div class="tp-caption small_title customin customout start"
                            data-x="center" data-hoffset="204"
                            data-y="290"
                            data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                            data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                            data-speed="1300"
                            data-start="800"
                            data-easing="Back.easeInOut"
                            data-endspeed="300"><?php echo $number['slider_text'];?>
                        </div>
                        
                        
                        <div class="tp-caption small_title customin cover-link customout start"
                            data-x="center" data-hoffset="180"
                            data-y="390"
                            data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                            data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                            data-speed="1600"
                            data-start="1400"
                            data-easing="Back.easeInOut"
                            data-endspeed="300"><a href="<?php the_field('contact_link','options');?>" class="btn btn-primary btn-lg"><?php echo $number['slider_button'];?> <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                        </div>
                        <div class="tp-caption customin customout"
                            data-x="left" data-hoffset="60"
                            data-y="bottom" data-voffset="70"
                            data-customin="x:50;y:150;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.5;scaleY:0.5;skewX:0;skewY:0;opacity:0;transformPerspective:0;transformOrigin:50% 50%;"
                            data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                            data-speed="800"
                            data-start="700"
                            data-easing="Power4.easeOut"
                            data-endspeed="500"
                            data-endeasing="Power4.easeIn"
                            style="z-index: 3"><img src="<?php echo $number['single-image']['url'];?>" alt="">
                        </div>
                        
                    </li>
                    <?php endforeach;?>
					<!-- <li data-transition="fade" data-slotamount="7" data-masterspeed="1500" >
                        <img src="demos/sliderbg_02.jpg"  alt="slidebg1"  data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">
                        <div class="tp-caption big_title_slider customin customout start"
                            data-x="center" data-hoffset="240"
                            data-y="170"
                            data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                            data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                            data-speed="1000"
                            data-start="500"
                            data-easing="Back.easeInOut"
                            data-endspeed="300">WELCOME TO JOLLY<span>MEDIC</span><br>
							EMERGENCY CARE
                        </div>
                        
                        <div class="tp-caption small_title customin customout start"
                            data-x="left" data-hoffset="485"
                            data-y="290"
                            data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                            data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                            data-speed="1300"
                            data-start="800"
                            data-easing="Back.easeInOut"
                            data-endspeed="300"><img src="images/slider_icon.png" alt=""> World class health care delivered with experience, expertise and empathy.
                        </div>

                        <div class="tp-caption small_title customin customout start"
                            data-x="left" data-hoffset="485"
                            data-y="330" 
                            data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                            data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                            data-speed="1300"
                            data-start="900"
                            data-easing="Back.easeInOut"
                            data-endspeed="300"><img src="images/slider_icon.png" alt=""> Jollymedic vision for the next phase of development is to 'Touch a Billion Lives'.
                        </div>

                        <div class="tp-caption small_title customin customout start"
                            data-x="left" data-hoffset="485"
                            data-y="370"
                            data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                            data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                            data-speed="1300"
                            data-start="1000"
                            data-easing="Back.easeInOut"
                            data-endspeed="300"><img src="images/slider_icon.png" alt=""> In the 30 years since, it has scripted one of the most magnificent stories of success 
                        </div>
                          
                        <div class="tp-caption small_title customin customout start"
                            data-x="center" data-hoffset="-40"
                            data-y="430"
                            data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                            data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                            data-speed="1600"
                            data-start="1100"
                            data-easing="Back.easeInOut"
                            data-endspeed="300"><a href="#" class="btn btn-dark btn-lg">Know More <i class="fas fa-chevron-right"></i></a>
                        </div>
                        
                        <div class="tp-caption small_title customin customout start"
                            data-x="center" data-hoffset="135"
                            data-y="430"
                            data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                            data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                            data-speed="1600"
                            data-start="1400"
                            data-easing="Back.easeInOut"
                            data-endspeed="300"><a href="#" class="btn btn-primary btn-lg">Make an Appoinment <i class="fas fa-chevron-right"></i></a>
                        </div>
                    </li>
					<li data-transition="fade" data-slotamount="7" data-masterspeed="1500" >
                        <img src="demos/sliderbg_01.jpg"  alt="slidebg1"  data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">
                        <div class="tp-caption big_title_slider customin customout start"
                            data-x="center" data-hoffset="200"
                            data-y="170"
                            data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                            data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                            data-speed="1000"
                            data-start="500"
                            data-easing="Back.easeInOut"
                            data-endspeed="300">JOLLY<span>MEDIC</span><br>
							CARE YOUR HEALTH
                        </div>
                        
                        <div class="tp-caption small_title customin customout start"
                            data-x="center" data-hoffset="204"
                            data-y="290"
                            data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                            data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                            data-speed="1300"
                            data-start="800"
                            data-easing="Back.easeInOut"
                            data-endspeed="300">Jollymedic is a prefect template for medical and health related projects or<br>
							 businesses. Easy to customizable and very easy to use!<br>
							 We Provide a Lifetime free update! for our template...
                        </div>
                        
                        <div class="tp-caption small_title customin customout start"
                            data-x="center" data-hoffset="12"
                            data-y="390"
                            data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                            data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                            data-speed="1600"
                            data-start="1100"
                            data-easing="Back.easeInOut"
                            data-endspeed="300"><a href="#" class="btn btn-dark btn-lg">Know More <i class="fas fa-chevron-right"></i></a>
                        </div>
                        
                        <div class="tp-caption small_title customin customout start"
                            data-x="center" data-hoffset="180"
                            data-y="390"
                            data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                            data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                            data-speed="1600"
                            data-start="1400"
                            data-easing="Back.easeInOut"
                            data-endspeed="300"><a href="#" class="btn btn-primary btn-lg">Make an Appoinment <i class="fas fa-chevron-right"></i></a>
                        </div>
                        
                        <div class="tp-caption customin customout"
                            data-x="left" data-hoffset="60"
                            data-y="bottom" data-voffset="70"
                            data-customin="x:50;y:150;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.5;scaleY:0.5;skewX:0;skewY:0;opacity:0;transformPerspective:0;transformOrigin:50% 50%;"
                            data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                            data-speed="800"
                            data-start="700"
                            data-easing="Power4.easeOut"
                            data-endspeed="500"
                            data-endeasing="Power4.easeIn"
                            style="z-index: 3"><img src="demos/slider_woman_01.png" alt="">
                        </div>
                    </li> -->
                 </ul>
			</div>
        </div>
	</div><!-- end slider-wrapper -->
        

    <?php 
                        $value = get_field('service_button');
                        if(!empty ($value)):
                            ?>
	<div class="white-wrapper nopadding">
    	<div class="container">
        	<div class="general-row">
                <div class="custom-services">

                            <?php
                            $args = array(
                            'post_type' => 'post',
                            'orderby' => 'date' ,
                            'order' => 'DESC' ,
                            'posts_per_page' => 4,
                            'cat'         => 'Services',
                            'paged' => get_query_var('paged'),
                            'post_parent' => $parent
                            );
                            $the_query = new WP_Query($args); ?>
                            <?php if ( $the_query->have_posts() ) : ?>
                            <?php while ( $the_query->have_posts() ) : $the_query->the_post();
                            // var_dump($post); exit();
                            // $categories = get_the_terms($post->ID,'event_categories'); ?>

                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 ">
						<div class="ch-item single-layer">	
							<div class="ch-info-wrap">
								<div class="ch-info">
									<div class="depart-ch ch-info-front">
                                    <?php the_field('service_icon');?>
                                        <h3><?php the_title(); ?></h3>
                                        <p><?php the_field('short_details'); ?></p>
                                    </div>
									<div class="depart-ch ch-info-back">
                                    <?php the_field('service_icon');?>
                                        <h3><?php the_title(); ?></h3>
                                        <p><?php the_field('short_details'); ?></p>
                                        <a class="readmore" href="<?php the_permalink() ?>" title="">Read More </a>
                                    </div>
								</div><!-- end ch-info -->
							</div><!-- end ch-info-wrap -->
						</div><!-- end ch-item -->
                    </div><!-- end col-sm-3 -->
                    <?php endwhile; ?>
                                <?php wp_reset_postdata(); ?>
                            <?php else : ?>
                                <p><?php esc_html_e( 'Sorry, no service found.' ); ?></p>
                            <?php endif; ?>
                         
                <div class="clearfix"></div>
                <div class="calloutbox">
                    <div class="col-lg-9 col-sm-12 text-service">
                        <h2><?php the_field('service_bar_text');?></h2>
                        <p><?php the_field('service_bar_sub_text');?></p>
                    </div>
                    <div class="col-lg-3 col-sm-12">
                        <a class="btn pull-right btn-dark btn-lg margin-top" href="<?php the_field('services_link','options');?>"><?php the_field('service_button'); ?> <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                    </div>
                </div><!-- end messagebox -->
                
				

            </div><!-- end row -->
        </div><!-- end container -->
    </div><!-- end white-wrapper -->
    <?php endif;?>
    



    <?php 
        $value = get_field('department_button_text');
        if(!empty ($value)):
            ?>
	<div class="grey-wrapper">
    	<div class="container">
        	<div class="general_row">
            	<div class="big-title clearfix">
                	<h3><?php the_field('department_title');?></h3>
                </div><!-- end big title -->	
                
				<div class="custom_tab_2 row">
                    <div class="col-md-4">
                        <ul class="nav nav-pills nav-stacked" id="another_tab">
                                <?php $terms = get_terms( array(
                            'post_type' => 'doctor',
                            'taxonomy' => 'doctor_categories',
                            
                            'has_archive' => true,
                            'public' => true,
                            'hierarchical' => false,
                            'hide_empty' => false,
                            ) );
                            $i = 0;
                                foreach ($terms as $term):
                                    // var_dump($term); exit();
                                    $slug = $term->slug;
                                    if ($i == 3) { break; }
                                    
                            
                            ?>

                            <li class="depart-name" ><a href="#<?php echo $slug; ?>" ><?php echo $term->name; ?></a></li>
                            <?php 
                            $i++;
                        endforeach; ?>
                        </ul>
                    </div>
                    
                    <div class="col-md-8">
                        <div class="tab-content ">
                            <?php $terms = get_terms( array(
                            'post_type' => 'doctor',
                            'taxonomy' => 'doctor_categories',
                            'has_archive' => true,
                            
                            'public' => true,
                            'hierarchical' => false,
                            'hide_empty' => false,
                            ) );
                            $i = 0;
                                foreach ($terms as $term):
                                    // var_dump($term); exit();
                                    
                                    $slug = $term->slug;
                                    if ($i == 3) { break; }
                            
                            ?>
                            <div class="tab-pane  depart-desc" id="<?php echo $slug; ?>">
                            	<img src="<?php the_field('iconic_image',$term); ?>" alt="" class="img-responsive alignleft">

								<p><?php the_field('department_detail', $term); ?> </p>
                            </div>
                            <?php 
                            $i++;
                         endforeach;?>
                        </div>
                    </div>
                </div>
                <a href="<?php the_field('department_link','options');?>" class="btn service-button"><?php the_field('department_button_text'); ?> <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
            </div><!-- end row -->
        </div><!-- end container -->
    </div><!-- end grey-wrapper -->
    <?php endif;?>




    <div class="white-wrapper">
    	<div class="container">
       		<div class="row">
  				<div class="col-lg-5 col-md-5 col-sm-6 col-xs-12">
                	<img src="<?php the_field('pathology_image');?>" alt="" class="img-responsive patho-image">
                </div><!-- end col -->
  				<div class="accord-bar col-lg-7 col-md-7 col-sm-6 col-xs-12">
                	<div class="general-title">
                        <h3><?php the_field('pathology_title');?></h3>
                    </div>
                    
					<p><?php the_field('pathology_description');?></p>

                                <div id="accordion-second" class="clearfix">
                                    <div class="accordion" id="accordion3">
                                    <?php
                                $args = array(
                                'post_type' => 'pathology',
                                // 'taxonomy' => 'property_categories',
                                // 'taxonomy' => 'property_locations',
                                // 'taxonomy' => 'property_rooms',
                                // 'taxonomy' => 'property_types',
                                'posts_per_page' => 2,
                                'count' => 2,
                                'hide_empty' => true,
                                );
                                $the_query = new WP_Query($args); ?>
                                <?php if ( $the_query->have_posts() ) : ?>
                                <?php while ( $the_query->have_posts() ) : $the_query->the_post(); 
                                    // $terms = get_the_terms($post, 'doctor_categories');
                                    
                                ?>

                                        <div class="accordion-group">
                                            <div class="accordion-heading ">
                                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#<?php the_ID(); ?>">
                                                    <em class="icon-fixed-width fa fa-plus"></em> <?php the_title();?>
                                                </a>
                                            </div>
                                            <div id="<?php the_ID(); ?>" class="accordion-body collapse ">
                                                <div class="accordion-inner">
                                                   <p><?php the_field('test_description');?></p>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <?php endwhile; ?>
                                <?php wp_reset_postdata(); ?>
                            <?php else : ?>
                                <p><?php esc_html_e( 'Sorry, no service found.' ); ?></p>
                            <?php endif; ?>
                        	                
                                    </div><!-- end accordion -->
                                </div><!-- end accordion first -->
                                <a href="<?php the_field('pathology_link','options');?>" class="btn service-button"><?php the_field('pathology_button_text');?><i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                </div><!-- end col -->             
            </div><!-- end general-row -->
        </div><!-- end container -->
    </div><!-- end grey-wrapper -->





    <?php 
        $value = get_field('about_button_text');
        if(!empty ($value)):
            ?>
    <div class="white-wrapper about-wrapper">
        <div class="container">
            <div class="big-title clearfix">
                	<h3><?php the_field('about_title');?></h3>
            </div><!-- end big title -->
            <div class="row">
                <div class="col-lg-6 col-md-12">
                    <div class="about-details">
                        <p><?php the_field('about_short_description');?></p>
                        <a href="<?php the_field('about_link','options');?>" class="btn about-view"><?php the_field('about_button_text');?> <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                    </div>
                </div>
                <div class="col-lg-6 col-md-12">
                    <div class="about-image">
                        <img src="<?php the_field('about-image');?>" >
                    </div>
                </div>
            </div>
            
        </div>
    </div>

    <?php endif;?>


    <?php 
                        $value = get_field('doctor_button_text');
                        if(!empty ($value)):
                            ?>
    <div class="grey-wrapper">
    	<div class="container">
        	<div class="general_row">
            	<div class="big-title clearfix">
                	<h3><?php the_field('doctor_title');?></h3>
                </div><!-- end big title -->
                
                <div class="carousel_widget">
					<div class="team_widget doc-view">
                    	<ul id="owl-blog" class="owl-carousel">
                                <?php
                                $args = array(
                                'post_type' => 'doctor',
                                // 'taxonomy' => 'property_categories',
                                // 'taxonomy' => 'property_locations',
                                // 'taxonomy' => 'property_rooms',
                                // 'taxonomy' => 'property_types',
                                'hide_empty' => true,
                                );
                                $the_query = new WP_Query($args); ?>
                                <?php if ( $the_query->have_posts() ) : ?>
                                <?php while ( $the_query->have_posts() ) : $the_query->the_post(); 
                                    $terms = get_the_terms($post, 'doctor_categories');
                                    
                                ?>
                        	<li class="spec-doc">
                                <div class="row">
                                    <div class="col-lg-4 col-12">
                                        <img src="<?php the_field('doctor_image');?>" class="img-responsive img-circle alignleft" alt=""> 
                                    </div>
                                    <div class="col-lg-8 col-12 doc-detailing">
                                        <h3><span><?php the_title();?></span></h3>
                                                    <?php
                                        // var_dump($terms);exit();
                                        foreach($terms as $term):
                                            
                                        ?>
                                        <h4><?php echo $term->name; ?></h4>
                                        <?php endforeach; ?>
                                        <p class="home-degree"><?php the_field('doctor_degree');?></p>
                                        <p><?php the_field('work'); ?></p>
                                        <p><?php the_field('time'); ?></p>
                                    </div>
                                </div>
                            	
                            </li>
                        	<?php endwhile; ?>
                                <?php wp_reset_postdata(); ?>
                            <?php else : ?>
                                <p><?php esc_html_e( 'Sorry, no service found.' ); ?></p>
                            <?php endif; ?>
                        	                 
                    	</ul>
                    </div><!-- end team_widget -->  
            	</div><!-- end carousel_widget -->     
            </div><!-- end general_row -->
            <a href="<?php the_field('doctor_link','options');?>" class="btn service-button"><?php the_field('doctor_button_text');?><i class="fa fa-arrow-right" aria-hidden="true"></i></a>
			
        </div><!-- end container -->
    </div><!-- end white-wrapper -->
    <?php endif;?>

	<?php 
                        $value = get_field('appointment_button_text','options');
                        if(!empty ($value)):
                            ?>
	
    <div class="white-wrapper">
        <div class="container">
            <div class="calloutbox">
                <div class="col-lg-9 col-sm-12">
                    <h2><?php the_field('appointment_text');?></h2>
                    <p><?php the_field('appointment_sub-text');?></p>
                </div>
                <div class="col-lg-3 col-sm-12">
                    <a class="btn pull-right btn-dark btn-lg margin-top" href="<?php the_field('contact_link','options');?>"><?php the_field('appointment_button_text','options');?> <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                </div>
            </div><!-- end messagebox -->
        </div>
    </div>
	
    <?php endif;?>


    <?php get_footer(); ?>
    