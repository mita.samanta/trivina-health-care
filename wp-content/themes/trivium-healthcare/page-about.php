<?php 
/*
* Template Name:About Page
*/
get_header();
?>
	<div class="shadow"></div>

	<div class="post-wrapper-top clearfix">
		<div class="container">
			<div class="col-lg-12">
				<h2><?php the_title();?></h2>
                <!-- <ul class="breadcrumb pull-right">
                    <li><a href="index-2.html">Home</a></li>
                    <li>About JollyMedic</li>
                </ul> -->
			</div>
		</div>
	</div><!-- end post-wrapper-top -->

	<div class="white-wrapper">
        	<div class="container">
            	<div class="row">
                	<div class="clearfix">
                    	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                			<div id="aboutslider" class="flexslider clearfix">
                                <ul class="slides">
                                    <?php $collects= get_field('about_slider');
                                    foreach ($collects as $collect ) :
                                        
                                        ?>
                                    
                                    <li><img class="img-responsive" src="<?php echo $collect['about_image']['url']?>" alt=""></li>
                                    <?php endforeach;?>
                                </ul><!-- end slides -->
                                <div class="aboutslider-shadow">
                                <span class="s1"></span>
                                </div>
							</div><!-- end slider -->
                        </div><!-- end col-lg-6 -->

                    	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        	<div class="title about-desc"><h2><?php the_field('trivium_healthcare_title');?></h2></div>
							<p><?php the_content();?></p>

							
                         
                         	<div class="clearfix"></div>
                            
                            <div class="about_icons row text-center">
                                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                    <div class="service-icon">
                                        <div class="dm-icon-effect-1">
                                            <a href="<?php the_field('services_link','options');?>" class=""> <?php the_field('service_icon');?> </a>
                                        </div>
                                        <div class="title"><h3><?php the_field('service_title');?></h3></div>
                                    </div>
                                </div>
    
                                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                    <div class="service-icon">
                                        <div class="dm-icon-effect-1">
                                            <a href="<?php the_field('doctor_link','options');?>" class=""><?php the_field('doctor_icon');?>  </a>
                                        </div>
                                        <div class="title"><h3><?php the_field('doctor_title');?></h3></div>
                                    </div>
                                </div>
                                
                                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                    <div class="service-icon">
                                        <div class="dm-icon-effect-1">
                                            <a href="<?php the_field('department_link','options');?>" class=""> <?php the_field('department_icon');?> </a>
                                        </div>
                                        <div class="title"><h3><?php the_field('department_title');?></h3></div>
                                    </div>
                                </div>
                            </div><!-- about_icons -->
                        </div><!-- end col-lg-6 -->
                	</div><!-- end fullwidth -->
                </div><!-- end row -->
            </div><!-- end container -->
    </div><!-- end white-wrapper -->
    
	<div class="grey-wrapper">
    	<div class="container">
       		<div class="row">
  				<div class="col-lg-5 col-md-5 col-sm-6 col-xs-12">
                	<img src="<?php the_field('about_single_image');?>" alt="" class="img-responsive">
                </div><!-- end col -->
  				<div class="col-lg-7 col-md-7 col-sm-6 col-xs-12 ">
                	<div class="general-title">
                        <h3 ><?php the_field('trivium_healthcare_text');?></h3>
                    </div>
                    
						<p></p>

                                <div id="accordion-second" class="clearfix">
                                    <div class="accordion" id="accordion3">
                                        <div class="accordion-group">
                                            <div class="accordion-heading active">
                                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseToggle1">
                                                    <em class="fa fa-minus icon-fixed-width"></em> <?php the_field('our_vision_title');?>
                                                </a>
                                            </div>
                                            <div id="collapseToggle1" class="accordion-body collapse in">
                                                <div class="accordion-inner">
                                                   <p><?php the_field('our_vision_details');?></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="accordion-group">
                                            <div class="accordion-heading">
                                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#collapseToggle2">
                                                    <em class="fa fa-plus"></em> <?php the_field('our_mission_title');?>
                                                </a>
                                            </div>
                                            <div id="collapseToggle2" class="accordion-body collapse">
                                                <div class="accordion-inner">
                                                   <p><?php the_field('our_mission_details');?></p>
                                                </div>
                                            </div>
                                        </div>
                                   
                                           
                                    </div><!-- end accordion -->
                                </div><!-- end accordion first -->
                                
                </div><!-- end col -->             
            </div><!-- end general-row -->
        </div><!-- end container -->
    </div><!-- end grey-wrapper -->

	<div class="white-wrapper">
    	<div class="container">
        	<div class="general_row">
            	<div class="big-title clearfix">
                	<h3>OUR DOCTORS</h3>
                </div><!-- end big title -->
                
                <div class="carousel_widget">
					<div class="team_widget doc-view">
                    	<ul id="owl-blog" class="owl-carousel">
                                <?php
                                $args = array(
                                'post_type' => 'doctor',
                                // 'taxonomy' => 'property_categories',
                                // 'taxonomy' => 'property_locations',
                                // 'taxonomy' => 'property_rooms',
                                // 'taxonomy' => 'property_types',
                                'hide_empty' => true,
                                );
                                $the_query = new WP_Query($args); ?>
                                <?php if ( $the_query->have_posts() ) : ?>
                                <?php while ( $the_query->have_posts() ) : $the_query->the_post(); 
                                    $terms = get_the_terms($post, 'doctor_categories');
                                    
                                ?>
                        	<li class="spec-doc">
                                <div class="row">
                                    <div class="col-lg-4 col-12">
                                        <img src="<?php the_field('doctor_image');?>" class="img-responsive img-circle alignleft" alt=""> 
                                    </div>
                                    <div class="col-lg-8 col-12 doc-detailing">
                                        <h3><span><?php the_title();?></span></h3>
                                                    <?php
                                        // var_dump($terms);exit();
                                        foreach($terms as $term):
                                            
                                        ?>
                                        <h4><?php echo $term->name; ?></h4>
                                        <?php endforeach; ?>
                                        <p class="home-degree"><?php the_field('doctor_degree');?></p>
                                        <p><?php the_field('work'); ?></p>
                                        <p><?php the_field('time'); ?></p>
                                        <button class="btn appoint about-doc" data-toggle="modal" id="clickme" data-target="#myModal-<?php the_ID(); ?>">Make an Appointment <i class="fa fa-arrow-right" aria-hidden="true"></i></button>
                                    </div>
                                </div>
                            	
                                  
                                
                            </li>
                        	<?php endwhile; ?>
                                <?php wp_reset_postdata(); ?>
                            <?php else : ?>
                                <p><?php esc_html_e( 'Sorry, no service found.' ); ?></p>
                            <?php endif; ?>
                        	                 
                    	</ul>
                    </div><!-- end team_widget -->  
            	</div><!-- end carousel_widget -->     
            </div><!-- end general_row -->
            
			
        </div><!-- end container -->
    </div><!-- end white-wrapper -->

    <?php
                                $args = array(
                                'post_type' => 'doctor',
                                // 'taxonomy' => 'property_categories',
                                // 'taxonomy' => 'property_locations',
                                // 'taxonomy' => 'property_rooms',
                                // 'taxonomy' => 'property_types',
                                'hide_empty' => true,
                                );
                                $the_query = new WP_Query($args); ?>
                                <?php if ( $the_query->have_posts() ) : ?>
                                <?php while ( $the_query->have_posts() ) : $the_query->the_post(); 
                                    $terms = get_the_terms($post, 'doctor_categories');
                                    
                                ?>



<!-- Modal -->
                            <div class="modal fade" id="myModal-<?php the_ID(); ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title" id="myModalLabel"></h4>

                                </div>
                                <div class="modal-body">
                                <h3 ><?php the_title(); ?></h3>

                                <?php
                                    // var_dump($terms);exit();
                                    foreach($terms as $term):
                                        
                                    ?>
                                <span><?php the_field('department_name','options');?> <h4><?php echo $term->name; ?></h4></span>
                                <?php endforeach; ?>
                                <?php 
                                $value = get_field('doctor_degree');
                                if(!empty ($value)):
                                    ?>
                                <span><?php the_field('degree','options');?> <h4><?php the_field('doctor_degree');?></h4></span>
                                <?php endif;?>
                                <?php 
                                
                                $value = get_field('doctor_experience');
                                if(!empty ($value)):
                                    ?>
                                <span><?php the_field('experience','options');?> <h4><?php the_field('doctor_experience'); ?></h4></span>
                                <?php endif;?>
                                <?php 
                                
                                $value = get_field('work');
                                if(!empty ($value)):
                                    ?>
                                <span><?php the_field('work','options');?>  <h4><?php the_field('work'); ?></h4></span>
                                <?php endif;?>
                                <?php 
                                
                                $value = get_field('time');
                                if(!empty ($value)):
                                    ?>
                                <span><?php the_field('time','options');?> <h4><?php the_field('time'); ?></h4></span>
                                <?php endif;?>
                                </div>


                                <form class="cwpp-ajax row online_form_builder_big" method="post"  action="#" id=" " >
                                    <input type="hidden" name="action" value="appointment">
                                    <input type="hidden" name="ufxd-doctor-name" id="ufxd-doctor-name" value="<?php the_title(); ?>">
                                    <div class="col-md-12">
                                        <label for="ufxd-name">Patient Name </label>
                                        <input type="text" class="form-control" id="ufxd-name" name="ufxd-name" placeholder="" required>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="ufxd-time">Time </label>
                                        <input type="time" class="form-control" id="ufxd-time" name="ufxd-time" required>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="ufxd-day">Day </label>
                                        <input type="date" class="form-control" id="ufxd-day" name="ufxd-day" required>
                                    </div>
                                    <div class="col-md-6">
                                    
                                        <label for="ufxd-email">Email Address </label>
                                        <input type="text" class="form-control" id="ufxd-email" name="ufxd-email">
                                    </div>
                                    <div class="col-md-6">
                                        <label for="ufxd-phone">Phone Number </label>
                                        <input type="text" class="form-control" id="ufxd-phone" name="ufxd-phone" required>
                                    </div>
                                    <div class="col-md-12">
                                        <label for="ufxd-message">Subject</label>
                                        <input type="text" class="form-control" id="ufxd-message" name="ufxd-message">
                                    </div>
                                    <!-- <div class="clearfix"></div>
                                    <div class="col-md-12">
                                    <label for="comments">Your Message</label>
                                    <textarea class="form-control" id="comments"></textarea>
                                    </div> -->
                                    <div class="clearfix"></div>
                                    <div class="col-md-12">
                                    <button type="submit"   class="btn btn-lg btn-primary pull-right">SEND NOW</button>   
                                    <div class="output"></div>
                                    </div>   
                                </form>

                                <!-- <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    <button type="button" class="btn btn-primary">Save changes</button>
                                </div> -->
                                </div><!-- /.modal-content -->
                            </div><!-- /.modal-dialog -->
                            </div><!-- /.modal -->
                            <?php endwhile; ?>
                                <?php wp_reset_postdata(); ?>
                            <?php else : ?>
                                <p><?php esc_html_e( 'Sorry, no service found.' ); ?></p>
                            <?php endif; ?> 
                            






    <?php 

get_footer();
?>