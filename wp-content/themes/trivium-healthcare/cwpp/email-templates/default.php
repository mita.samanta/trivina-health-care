<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
</head>
<body>
    <h2>{{title}}</h2>
    <p><strong>Name:</strong> {{name}}</p>
    <p><strong>Email Id:</strong> {{email}}</p>
    <p><strong>Subject:</strong> {{subject}}</p>
    <p>{{message}}</p>
</body>
</html>