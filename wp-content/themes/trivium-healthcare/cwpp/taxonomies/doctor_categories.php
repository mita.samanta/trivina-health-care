<?php
class doctor_categories extends CWPPTaxonomy{
    public function __construct()
    {
        $this->labelName = "Doctor Categories";
        $this->labelNameSingular = "Doctor Category";
        $this->hierarchical = true;
        $this->rewrite = true;
        $this->rewriteSlug= "doctor-category";
        $this->postTypes = ['doctor'];
        $this->hierarchical = true;
    }
}
?>