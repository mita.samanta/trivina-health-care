<?php

return [

    CWPP_CFG_STYLES => [

       'bootstrap' => 'css/bootstrap.css',
        'jqueryui' => '//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css',
        'googlefont' => 'fonts.googleapis.com/csseefa.css?family=Lato:400,400italic,700,700italic,900,300' ,
        'googlefont' => 'fonts.googleapis.com/cssa691.css?family=Roboto:400,300,300italic,400italic,500,500italic,700,700italic,900' ,
        'fontawesome' => 'css/font-awesome.css',
       'light-box' => 'https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.11.1/css/lightbox.min.css',
        'owl' => 'css/owl-carousel.css',
        'lightgallery' => 'https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.7.2/css/lightgallery.min.css',
        'flex-slider' => 'css/flexslider.css',
        'pretty-photo' => 'css/prettyPhoto.css',
        'slick' => 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.css',
        
        'slicktheme' => 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick-theme.min.css',
        'rs-plugin' => 'rs-plugin/css/settings.css',
        
        'style' => 'style.css'

    ],

    CWPP_CFG_SCRIPTS => [
        'jquery' => 'js/jquery.js',
        'lightgallery' => 'https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.7.2/js/lightgallery-all.min.js',
        'light-box' => 'https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.11.1/js/lightbox.min.js',
        'slick' => 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js',
        // 'fit-vids' => 'js/jquery.fitvids.js',
        'isotope' => 'js/jquery.isotope.min.js',
        'jquery-ui' => 'http://code.jquery.com/ui/1.10.2/jquery-ui.js',
        
        'flex-slider' => 'js/jquery.flexslider.js',
        'pretty-photo' => 'js/jquery.prettyPhoto.js',
        'js-menu' => 'js/menu.js',
        'ratina' => 'js/retina-1.1.0.html',
        'bootstrap' => 'js/bootstrap.min.js',
        'bootstrap-datetime' => 'js/bootstrap-datetimepicker.js',
        'custom' => 'js/custom.js',
       
        'owl' => 'js/owl.carousel.min.js',
        'revolution' => 'rs-plugin/js/jquery.themepunch.revolution.min.js',
        'themepunch' => 'rs-plugin/js/jquery.themepunch.plugins.min.js',
        'lightgallery' => 'https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.6.12/js/lightgallery.min.js',
        'app' => 'src/js/app.js',
        'map' => 'https://maps.googleapis.com/maps/api/js?key=AIzaSyAgnT0SI2NPcpVLH3QRacFbNk1S2xOIdzo&callback=initMap'
    ],

    CWPP_CFG_THEME_SUPPORTS => [

        'post-thumbnails' => array('post', 'page'),
        'title-tag' => true,

    ],

    CWPP_CFG_MENUS => [

        'primary' => 'Primary Menu',
        'services' => 'Services',
//        'quick-links' => 'Quick Links',

    ],

    CWPP_CFG_EMAIL => [
        'type' => CWPP_EMAIL_TYPE_HTML
    ]


];