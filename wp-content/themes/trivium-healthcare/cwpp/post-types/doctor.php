<?php
class doctor extends CWPPPostType{
    public function __construct(){
        $this->labelName = "Doctors";
        $this->labelNameSingular = "Doctor";
        $this->hasArchive = "doctors";
        $this->rewriteSlug= "doctor";
        $this->supportsThumbnail = true;
    }
}