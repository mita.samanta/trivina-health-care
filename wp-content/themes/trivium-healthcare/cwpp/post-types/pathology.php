<?php
class pathology extends CWPPPostType{
    public function __construct(){
        $this->labelName = "Pathologies";
        $this->labelNameSingular = "Pathology";
        $this->hasArchive = "pathologies";
        $this->rewriteSlug= "pathology";
        $this->supportsThumbnail = true;
    }
}