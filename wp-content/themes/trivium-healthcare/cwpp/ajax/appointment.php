<?php

$cf = new CWPPAjaxForm( $_POST, ['ufxd-doctor-name','ufxd-name', 'ufxd-time', 'ufxd-day', 'ufxd-email','ufxd-phone','ufxd-message'], array(
    'ufxd-email' => 'is_email'
));
$cf->validateFields();



if( $cf->hasError() ){
    $cf->error( '<i class="fa fa-close"></i> Please check the highlighted fields.' );
}else{

    $email = new CWPPEmailTemplate('appointment');
    if($email->send( get_option('admin_email'), 'Contact from '.get_bloginfo('name'), [
        'title' => 'A user has booked you form your website '.get_bloginfo('name'),
        'doctor-name' => $_POST['ufxd-doctor-name'],
        'name' => $_POST['ufxd-name'],
        'time' => $_POST['ufxd-time'],
        'day' => $_POST['ufxd-day'],
        'email' => $_POST['ufxd-email'],
        'phone' => $_POST['ufxd-phone'],
        'message' => $_POST['ufxd-message']
    ] )){
        $cf->success( '<i class="fa fa-check mr-1"></i> Thank you for Reaching us.' );
    }else{
        $cf->error( '<i class="fas fa-exclamation-circle mr-1"></i> Something bad has happen, Please try again later.' );
    }

    // DO THE JOB ==============


}


$cf->response();