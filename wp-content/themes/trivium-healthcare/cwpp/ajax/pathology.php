<?php

$cf = new CWPPAjaxForm( $_POST, ['ufxd-pathology-name','ufxd-path-name', 'ufxd-path-time', 'ufxd-path-day', 'ufxd-path-email','ufxd-path-phone','ufxd-path-message'], array(
    'ufxd-email' => 'is_email'
));
$cf->validateFields();



if( $cf->hasError() ){
    $cf->error( '<i class="fa fa-close"></i> Please check the highlighted fields.' );
}else{

    $email = new CWPPEmailTemplate('pathology');
    if($email->send( get_option('admin_email'), 'Contact from '.get_bloginfo('name'), [
        'title' => 'A user has booked you form your website '.get_bloginfo('name'),
        'pathology-name' => $_POST['ufxd-pathology-name'],
        'name' => $_POST['ufxd-path-name'],
        'time' => $_POST['ufxd-path-time'],
        'day' => $_POST['ufxd-path-day'],
        'email' => $_POST['ufxd-path-email'],
        'phone' => $_POST['ufxd-path-phone'],
        'message' => $_POST['ufxd-path-message']
    ] )){
        $cf->success( '<i class="fa fa-check mr-1"></i> Thank you for Reaching us.' );
    }else{
        $cf->error( '<i class="fas fa-exclamation-circle mr-1"></i> Something bad has happen, Please try again later.' );
    }

    // DO THE JOB ==============


}


$cf->response();