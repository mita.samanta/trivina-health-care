<?php

$cf = new CWPPAjaxForm( $_POST, ['ufxd-contact-name', 'ufxd-contact-email',  'ufxd-contact-subject','ufxd-contact-message'], array(
    'ufxd-email' => 'is_email'
));
$cf->validateFields();



if( $cf->hasError() ){
    $cf->error( '<i class="fa fa-close"></i> Please check the highlighted fields.' );
}else{

    

$email = new CWPPEmailTemplate();
if($email->send( get_option('admin_email'), 'Contact from '.get_bloginfo('name'), [
    'title' =>'Someone has contact you form your website '.get_bloginfo('name'),
    'name' => $_POST['ufxd-contact-name'],
    'email' => $_POST['ufxd-contact-email'],
    'subject' => $_POST['ufxd-contact-subject'],
    'message' => $_POST['ufxd-contact-message']
    
] )){
    $cf->success( '<i class="fa fa-check"></i> Thank you for contacting us.' );
}else{
    $cf->error( '<i class="fa fa-close"></i> Something bad is happened, please try again later');
}






    // DO THE JOB ==============


}


$cf->response();