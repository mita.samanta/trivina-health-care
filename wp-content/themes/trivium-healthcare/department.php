<?php 
/*
* Template Name: Departments-Archive Page
*/
get_header();
?>
	<div class="shadow"></div>

	<div class="post-wrapper-top clearfix">
		<div class="container">
			<div class="col-lg-12">
				<h2><?php the_title();?></h2>
                <!-- <ul class="breadcrumb pull-right">
                    <li><a href="index-2.html">Home</a></li>
                    <li>Deparments</li>
                </ul> -->
			</div>
		</div>
	</div><!-- end post-wrapper-top -->


    <div class="white-wrapper">
    	<div class="container">
        	<div class="general-row">
            	<div class="general-title text-center">
                	<h3><?php the_field('service_title');?></h3>
                    <p class="lead"><?php the_field('service_text');?></p>
                </div><br>
             
                <div class="custom-services">

                <?php $terms = get_terms( array(
                            'post_type' => 'doctor',
                            'taxonomy' => 'doctor_categories',
                            'has_archive' => true,
                            'public' => true,
                            'hierarchical' => false,
                            'hide_empty' => false,
                            ) );
                        
                                foreach ($terms as $term):
                                    // var_dump($term); exit();
                                    $slug = $term->slug;
                            
                            ?>

                    <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 ">
						<div class="ch-item single-layer depart-layer">	
							<div class="ch-info-wrap">
								<div class="ch-info ">
									<div class="depart-ch ch-info-front">
                                  
                                        <img src="<?php the_field('iconic_image',$term); ?>" alt="" class="img-responsive alignleft">
                                        <h3><?php echo $term->name; ?></h3>
                                        <p><?php the_field('small-desc', $term); ?></p>
                                    </div>
									<div class="depart-ch ch-info-back ">
                                        <a href="<?php echo get_term_link( $term->term_id ) ?>">
                                            <img src="<?php the_field('iconic_image',$term); ?>" alt="" class="img-responsive alignleft">
                                            <h3 class="depart-text"><?php echo $term->name; ?></h3>
                                            <p><?php the_field('small-desc', $term); ?></p>
                                            <p class="readmore"  >Read More </p>
                                        </a>
                                    </div>
								</div><!-- end ch-info -->
							</div><!-- end ch-info-wrap -->
						</div><!-- end ch-item -->
                    </div><!-- end col-sm-3 -->
                    <?php endforeach; ?>
                    
                    
                </div><!-- end custom-services -->
                
                <div class="clearfix"></div>
                
				
 
            </div><!-- end row -->
        </div><!-- end container -->
    </div><!-- end white-wrapper -->


	

	<div class="grey-wrapper">
    	<div class="container">
        	<div class="general_row">
            	<div class="big-title clearfix">
                	<h3>OUR DOCTORS</h3>
                </div><!-- end big title -->
                
                <div class="carousel_widget">
					<div class="team_widget below doc-view">
                    	<ul id="owl-blog" class="owl-carousel">
                                <?php
                                $args = array(
                                'post_type' => 'doctor',
                                // 'taxonomy' => 'property_categories',
                                // 'taxonomy' => 'property_locations',
                                // 'taxonomy' => 'property_rooms',
                                // 'taxonomy' => 'property_types',
                                'hide_empty' => true,
                                );
                                $the_query = new WP_Query($args); ?>
                                <?php if ( $the_query->have_posts() ) : ?>
                                <?php while ( $the_query->have_posts() ) : $the_query->the_post(); 
                                    $terms = get_the_terms($post, 'doctor_categories');
                                    
                                ?>
                        	<li class="spec-doc">
                                <div class="row">
                                    <div class="col-lg-4 col-12">
                                        <img src="<?php the_field('doctor_image');?>" class="img-responsive img-circle alignleft" alt=""> 
                                    </div>
                                    <div class="col-lg-8 col-12 doc-detailing">
                                        <h3><span><?php the_title();?></span></h3>
                                                    <?php
                                        // var_dump($terms);exit();
                                        foreach($terms as $term):
                                            
                                        ?>
                                        <h4><?php echo $term->name; ?></h4>
                                        <?php endforeach; ?>
                                        <p class="home-degree"><?php the_field('doctor_degree');?></p>
                                        <p><?php the_field('work'); ?></p>
                                        <p><?php the_field('time'); ?></p>
                                        <button class="btn appoint depart-doc" data-toggle="modal" id="clickme" data-target="#myModal-<?php the_ID(); ?>">Make an Appointment <i class="fa fa-arrow-right" aria-hidden="true"></i></button>
                                    </div>
                                </div>
                            	
                                  
                                
                            </li>
                        	<?php endwhile; ?>
                                <?php wp_reset_postdata(); ?>
                            <?php else : ?>
                                <p><?php esc_html_e( 'Sorry, no service found.' ); ?></p>
                            <?php endif; ?>
                        	                 
                    	</ul>
                    </div><!-- end team_widget -->  
            	</div><!-- end carousel_widget -->     
            </div><!-- end general_row -->

			
        </div><!-- end container -->
    </div><!-- end white-wrapper -->



    <?php
                                $args = array(
                                'post_type' => 'doctor',
                                // 'taxonomy' => 'property_categories',
                                // 'taxonomy' => 'property_locations',
                                // 'taxonomy' => 'property_rooms',
                                // 'taxonomy' => 'property_types',
                                'hide_empty' => true,
                                );
                                $the_query = new WP_Query($args); ?>
                                <?php if ( $the_query->have_posts() ) : ?>
                                <?php while ( $the_query->have_posts() ) : $the_query->the_post(); 
                                    $terms = get_the_terms($post, 'doctor_categories');
                                    
                                ?>



<!-- Modal -->
                            <div class="modal fade" id="myModal-<?php the_ID(); ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title" id="myModalLabel"></h4>

                                </div>
                                <div class="modal-body">
                                <h3 ><?php the_title(); ?></h3>

                                <?php
                                    // var_dump($terms);exit();
                                    foreach($terms as $term):
                                        
                                    ?>
                                <span><?php the_field('department_name','options');?> <h4><?php echo $term->name; ?></h4></span>
                                <?php endforeach; ?>
                                <?php 
                                $value = get_field('doctor_degree');
                                if(!empty ($value)):
                                    ?>
                                <span><?php the_field('degree','options');?> <h4><?php the_field('doctor_degree');?></h4></span>
                                <?php endif;?>
                                <?php 
                                
                                $value = get_field('doctor_experience');
                                if(!empty ($value)):
                                    ?>
                                <span><?php the_field('experience','options');?> <h4><?php the_field('doctor_experience'); ?></h4></span>
                                <?php endif;?>
                                <?php 
                                
                                $value = get_field('work');
                                if(!empty ($value)):
                                    ?>
                                <span><?php the_field('work','options');?>  <h4><?php the_field('work'); ?></h4></span>
                                <?php endif;?>
                                <?php 
                                
                                $value = get_field('time');
                                if(!empty ($value)):
                                    ?>
                                <span><?php the_field('time','options');?> <h4><?php the_field('time'); ?></h4></span>
                                <?php endif;?>
                                </div>


                                <form class="cwpp-ajax row online_form_builder_big" method="post"  action="#" id=" " >
                                    <input type="hidden" name="action" value="appointment">
                                    <input type="hidden" name="ufxd-doctor-name" id="ufxd-doctor-name" value="<?php the_title(); ?>">
                                    <div class="col-md-12">
                                        <label for="ufxd-name">Patient Name </label>
                                        <input type="text" class="form-control" id="ufxd-name" name="ufxd-name" placeholder="" required>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="ufxd-time">Time </label>
                                        <input type="time" class="form-control" id="ufxd-time" name="ufxd-time" required>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="ufxd-day">Day </label>
                                        <input type="date" class="form-control" id="ufxd-day" name="ufxd-day" required>
                                    </div>
                                    <div class="col-md-6">
                                    
                                        <label for="ufxd-email">Email Address </label>
                                        <input type="text" class="form-control" id="ufxd-email" name="ufxd-email">
                                    </div>
                                    <div class="col-md-6">
                                        <label for="ufxd-phone">Phone Number </label>
                                        <input type="text" class="form-control" id="ufxd-phone" name="ufxd-phone" required>
                                    </div>
                                    <div class="col-md-12">
                                        <label for="ufxd-message">Subject</label>
                                        <input type="text" class="form-control" id="ufxd-message" name="ufxd-message">
                                    </div>
                                    <!-- <div class="clearfix"></div>
                                    <div class="col-md-12">
                                    <label for="comments">Your Message</label>
                                    <textarea class="form-control" id="comments"></textarea>
                                    </div> -->
                                    <div class="clearfix"></div>
                                    <div class="col-md-12">
                                    <button type="submit"   class="btn btn-lg btn-primary pull-right">SEND NOW</button>   
                                    <div class="output"></div>
                                    </div>   
                                </form>

                                <!-- <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    <button type="button" class="btn btn-primary">Save changes</button>
                                </div> -->
                                </div><!-- /.modal-content -->
                            </div><!-- /.modal-dialog -->
                            </div><!-- /.modal -->
                            <?php endwhile; ?>
                                <?php wp_reset_postdata(); ?>
                            <?php else : ?>
                                <p><?php esc_html_e( 'Sorry, no service found.' ); ?></p>
                            <?php endif; ?> 
    
   <?php get_footer();?>