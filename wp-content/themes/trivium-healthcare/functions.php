<?php
/**
 * General Configurations & Constants
 * DO NOT CHANGE ANYTHING HERE
 */

if( !defined( 'ABSPATH' ) ) exit;

require_once get_template_directory() . '/functions/class-wp-bootstrap-navwalker.php';



if( function_exists('acf_add_options_sub_page') ) {

    if (current_user_can('manage_options')){

        acf_add_options_sub_page('General');
        acf_add_options_sub_page('Contact');
        acf_add_options_sub_page('Social');
    }else{

        add_action('admin_menu', function (){
            remove_menu_page('acf-options');
        }, 1000);

    }
}

// function searchfilter($query) {
//     if ($query->is_search && !is_admin() ) {
//         if(isset($_GET['post_type'])) {
//             $type = $_GET['post_type'];
//                 if($type == 'propert') {
//                     $query->set('post_type',array('property'));
//                 }
//         }       
//     }
// return $query;
// }
// add_filter('pre_get_posts','searchfilter');

// function template_chooser($template)   
// {    
//   global $wp_query; 
//   $post_type = $wp_query->query_vars["pagename"];   
//   if( isset($_GET['s']) && $post_type == 'property' )   
//   {

//     return locate_template('archive.php');  //  redirect to archive-search.php
//   }   
//   return $template;   
// }
// add_filter('template_include', 'template_chooser'); 



function buildSelect($tax){ 
    $terms = get_terms($tax);
    $x = '<select multiple="multiple" name="'. $tax .'">';
    $x .= '<option value="">Select '. ucfirst($tax) .'</option>';
    foreach ($terms as $term) {
       $x .= '<option value="' . $term->slug . '">' . $term->name . '</option>';    
    }
    $x .= '</select>';
    return $x;
    }



