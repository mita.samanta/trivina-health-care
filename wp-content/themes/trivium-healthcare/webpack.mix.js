const mix = require('laravel-mix');







// mix.js('src/js/app.js', 'assets/js/app.js').sourceMaps();
mix.sass('src/scss/style.scss', 'style.css').options({
    processCssUrls: false
});


if (!mix.inProduction()) {

    mix.webpackConfig({devtool: 'source-map'}).sourceMaps(); // Source Maps for assets.

}
