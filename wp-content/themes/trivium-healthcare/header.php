<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from jollythemes.com/html/jollymedic/index1.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 22 Jun 2020 12:34:48 GMT -->
<head>
  
  <meta http-equiv="content-type" content="text/html; charset=UTF-8"> 

  

  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
  <meta name="description" content="">
  <meta name="keywords" content="">
  <meta name="author" content="">
  <title> <?php wp_title(); ?></title>
    <?php wp_head();?>
 
  <!-- SLIDER REVOLUTION 4.x CSS SETTINGS -->
  
  

</head>
<body>

<div class="animationload">
<div class="loader">Loading...</div>
</div>

    <div class="topbar">
    	<div class="container">
        	<div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12">
                    <div class="callout">
                        <span class="topbar-email"><i class="fa fa-envelope"></i> <a href="mailto:name@yoursite.com"><?php the_field('email','options');?></a></span>
                        <span class="topbar-phone"><i class="fa fa-phone"></i> Call us: <?php the_field('phone','options');?></span>
                    </div><!-- end callout -->
                </div><!-- end col -->
                <div class="col-lg-6 col-md-6 col-sm-12">
                    <div class="topbar_social pull-right">

                        <?php 
                        $value = get_field('facebook' , 'options');
                        if(!empty ($value)):
                            ?>
						<span class="facebook"><a data-toggle="tooltip" data-placement="bottom" title="Facebook" href="<?php the_field('facebook','options');?>"><i class="fa fa-facebook"></i></a></span>
                        <?php endif;?>

                        <?php 
                        $value = get_field('google-plus' , 'options');
                        if(!empty ($value)):
                            ?>
						<span class="google-plus"><a data-toggle="tooltip" data-placement="bottom" title="Google Plus" href="<?php the_field('google-plus','options');?>"><i class="fa fa-google-plus"></i></a></span>
                        <?php endif;?>

                        <?php 
                        $value = get_field('twitter' , 'options');
                        if(!empty ($value)):
                            ?>
						<span class="twitter"><a data-toggle="tooltip" data-placement="bottom" title="Twitter" href="<?php the_field('twitter','options');?>"><i class="fa fa-twitter"></i></a></span>
                        <?php endif;?>

                        <?php 
                        $value = get_field('linkedin' , 'options');
                        if(!empty ($value)):
                            ?>
						<span class="linkedin"><a data-toggle="tooltip" data-placement="bottom" title="Linkedin" href="<?php the_field('linkedin','options');?>"><i class="fa fa-linkedin"></i></a></span>
                        <?php endif;?>

                        <?php 
                        $value = get_field('skype' , 'options');
                        if(!empty ($value)):
                            ?>
						<span class="skype"><a data-toggle="tooltip" data-placement="bottom" title="Skype" href="<?php the_field('skype','options');?>"><i class="fa fa-skype"></i></a></span>
                        <?php endif;?>
					</div><!-- end social icons -->
                </div><!-- end col -->
            </div><!-- end row -->
        </div><!-- end container -->
    </div><!-- end topbar -->
    
	<header class="header">
		<div class="container-fluid">
			<nav class="navbar yamm navbar-default">
				<div class="navbar-header">
                    <button type="button" data-toggle="collapse" data-target="#navbar-collapse-1" class="navbar-toggle">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a href="<?php the_field('logo_link','options');?>" class="navbar-brand"><img src="<?php the_field('logo_image','options');?>" alt="Jollymedic"></a>
        		</div><!-- end navbar-header -->
                
				<div id="navbar-collapse-1" class="navbar-collapse collapse navbar-right">

                            <?php
                                wp_nav_menu([
                                    'theme_location' => 'primary',
                                    'container' => 'ul',
                                    'menu_class' => 'nav navbar-nav',
                                    'depth' => 2,
                                    'fallback_cb'     => 'WP_Bootstrap_Navwalker::fallback',
                                    'walker'          => new WP_Bootstrap_Navwalker(),
                                ]);
                            ?>

                            
					<!-- <ul class="nav navbar-nav">
                        <li class="dropdown"><a href="#" data-toggle="dropdown" class="dropdown-toggle">Home </a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="index1.html">Homepage 1</a></li>
                                <li><a href="index2.html">Homepage 2</a></li>
                                <li><a href="index3.html">Homepage 3</a></li>
                                <li><a href="index4.html">Homepage 4</a></li>
                                <li><a href="#">Build your own!</a></li>
                            </ul>
                        </li>
                        <li><a href="doctors.html">Doctors</a></li>
                        <li><a href="appointment.html">Appointment</a></li>
                        <li class="dropdown"><a href="#" data-toggle="dropdown" class="dropdown-toggle">Shop </a>
                            <ul class="dropdown-menu" role="menu">
								<li><a href="shop1.html">Shop Fullwidth</a></li>
								<li><a href="shop2.html">Shop with Sidebar</a></li>
								<li><a href="shop-single-sidebar.html">Single Shop Sidebar</a></li>
								<li><a href="shop-single-fullwidth.html">Single Shop Fullwidth</a></li>
								<li><a href="shop-checkout.html">Checkout</a></li>
								<li><a href="shop-cart.html">Shopping Cart</a></li>              
                            </ul>
                        </li>
						<li class="dropdown yamm-fw"><a href="#" data-toggle="dropdown" class="dropdown-toggle">Mega Menu </a>
							<ul class="dropdown-menu">
                                <li>
                                    <div class="yamm-content">
                                        <div class="row">
                                           <ul class="col-sm-3">
                                                <li><a href="services-one.html">Services Style 1</a></li>
                                                <li><a href="services-two.html">Services Style 2</a></li>
                                                <li><a href="services-three.html">Services Style 3</a></li>
                                                <li><a href="departmens.html">Departments</a></li>
                                                <li><a href="departmen-single.html">Departmen Single</a></li>
												<li><a href="appointment.html">Appointment Form</a></li>
                                                <li><a href="hospital-staffs.html">Hospital Staffs</a></li>
                                                <li><a href="hospital-doctors.html">Hospital Doctors</a></li>
                                                <li><a href="hospital-doctor-single.html">Doctor Single</a></li>
                                           </ul>
                                           <ul class="col-sm-3">
                                                <li><a href="blog-one.html">Blog Style 1</a></li>
                                                <li><a href="blog-two.html">Blog Style 2</a></li>
                                                <li><a href="blog-single.html">Blog Single</a></li>
                                                <li><a href="about.html">About Us</a></li>
                                                <li><a href="gallery-one.html">Gallery Style 1</a></li>
                                                <li><a href="gallery-two.html">Gallery Style 2</a></li>
                                                <li><a href="gallery-three.html">Gallery Style 3</a></li>
                                                <li><a href="gallery-four.html">Gallery Style 4</a></li>
                                                <li><a href="gallery-single.html">Gallery Single</a></li>
                                           </ul>
                                           <ul class="col-sm-3">
                                                <li><a href="timeline.html">Hospital Timeline</a></li>
                                                <li><a href="testimonials.html">Testimonials</a></li>
                                                <li><a href="sitemap.html">Sitemap</a></li>
                                                <li><a href="404.html">Not Found</a></li>
                                                <li><a href="faqs.html">FAQs</a></li>
                                                <li><a href="contact.html">Contact us</a></li>
                                                <li><a href="page.html">Page with Sidebar</a></li>
                                                <li><a href="page-fullwidth.html">Page Fullwidth</a></li>
                                                <li><a href="typography.html">Typography</a></li>
                                           </ul>
                                           <ul class="col-sm-3">
                                                <li><a href="accordion-toggle.html">Accordions</a></li>
                                                <li><a href="buttons.html">Buttons</a></li>
                                                <li><a href="form-elements.html">Form Elements</a></li>
                                                <li><a href="tables.html">Table Elements</a></li>
                                                <li><a href="tabs.html">Tab Elements</a></li>
                                                <li><a href="alerts.html">Alert Elements</a></li>
                                                <li><a href="animate-scripts.html">Animated Counters</a></li>
                                                <li><a href="progress-bars.html">Progress Bars</a></li>
                                                <li><a href="pricing-tables.html">Pricing Tables</a></li>
                                           </ul>
                                        </div>
                                    </div>
                                </li>
                            </ul>
						</li>
                        <li class="active"><a href="contact.html">Contact</a></li>
                        <li class="last_item"><a href="https://www.jollythemes.com/html/jollymedic/shop.html"><i class="fa fa-shopping-cart"></i></a></li>
					</ul> -->
				</div><!-- #navbar-collapse-1 -->
			</nav><!-- end navbar yamm navbar-default -->
		</div><!-- end container -->
    </header><!-- end header -->
    