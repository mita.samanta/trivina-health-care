



$(document).ready(function(){
    $(".depart-name:first-child").addClass("active");
    $(".depart-desc:first-child").addClass("active");
    
    $(" .accordion-heading:first-child").addClass("active");
    $(".accordion-body:first-child").addClass("in");
    $(".pharma-icon > i").addClass("hovicon ");
    $(".pharma-icon > i").addClass("sub-a ");
    $(".pharma-icon > i").addClass("effect-1 ");
    $(".pharma-icon > i").addClass(" fa-2x");

    
    // $('.accordion-toggle').click(function(){
    //     //Close all
    //     $(".accordion-body").each(function(){
    //       $(this).slideUp('fast');
    //     })
        
    //      $(this).next().slideToggle('fast');
  
    //   });
    $('.sliders').slick({
        infinite: true,
       dots:false,
       speed: 500,
        fade: true
        // autoplay:true,
        // cssEase: 'linear'
      });
      
    $("#clickme").on
      ("click", function(){ $("#myModal-" + $(this).attr('data-id')).modal();});
    // $(".depart-name").click(function(){  
    //     $(".depart-desc:first-child").removeClass("active");
    //     $(".depart-desc:last-child").addClass("active");
        
        
    // });
    $('.sliders').lightGallery({
        selector: '.slick-slide:not(.slick-cloned) .lightgallery',
      });
   
    $('.slides').lightGallery({
        selector: '.flex-active-slide:not(.clone) .lightgallery',
      });

    
    $('.flexslider').flexslider({
        animation: "slide"
      });

      

    (function($) {
        "use strict";
          
      })(jQuery);


      (function($) {
        "use strict";
          
         	//ready
      })(jQuery);

      (function($) {
        "use strict";
          // OWL Carousel
          $("#owl-blog").owlCarousel({
              items : 3,
              lazyLoad : true,
              navigation : true,
              pagination : false,
              autoPlay: false
          });
          var revapi;
         
              revapi = jQuery('.tp-banner').revolution(
              {
                  delay:9000,
                  startwidth:1170,
                  startheight:560,
                  hideThumbs:10,
                  fullWidth:"on",
                  forceFullWidth:"on"
              });
              $('.form_datetime').datetimepicker({
                weekStart: 1,
                todayBtn:  1,
                autoclose: 1,
                todayHighlight: 1,
                startView: 2,
                forceParse: 0,
                showMeridian: 1
            });
      })(jQuery);


     
     
    $('.specialist-slider').slick({
        infinite: true,
        lazyLoad: 'ondemand',
  slidesToShow: 2,
  slidesToScroll: 2,
        dots:false,
        responsive: [
          {
            breakpoint: 768,
            settings: {
              arrows: false,
              centerMode: true,
              centerPadding: '40px',
              slidesToShow: 2
            }
          },
          {
            breakpoint: 480,
            settings: {
              arrows: false,
              centerMode: true,
              centerPadding: '40px',
              slidesToShow: 1
            }
          }
        ]
      });


  
  

      $(".single-click").click(function(){  
        
        $(".scroll").addClass("active");  
        
    }); 
      
      

    
    


    $( function() {
        $( "#slider-range" ).slider({
          range: true,
          min: 5000,
          max: 50000,
          values: [ 10000, 30000 ],
          slide: function( event, ui ) {
            $( "#amount" ).val( "₹" + ui.values[ 0 ] + " - ₹" + ui.values[ 1 ] );
          }
        });
        $( "#amount" ).val( "₹" + $( "#slider-range" ).slider( "values", 0 ) +
          " - ₹" + $( "#slider-range" ).slider( "values", 1 ) );
      } );




  
    $(window).scroll(function() {
		var scrollDistance = $(window).scrollTop();

		// Show/hide menu on scroll
		//if (scrollDistance >= 850) {
		//		$('nav').fadeIn("fast");
		//} else {
		//		$('nav').fadeOut("fast");
		//}
	
		// Assign active class to nav links while scolling
		$('.view-item').each(function(i) {
				if ($(this).position().top <= scrollDistance) {
						$('.single-option a.active').removeClass('active');
						$('.single-option a').eq(i).addClass('active');
				}
		});
}).scroll();
    

  
   


    //   var name = document.getElementById('zead');
    //   var nameId = name.getAttribute('data-head-type');
    //   var p = 0;
    //   var next =  nameId;
    //   var sped = 50;
      
    //   function headWrite() {
    //     if (p < next.length) {
    //       document.getElementById("zead").innerHTML += next.charAt(p);
    //       p++;
    //       setTimeout(headWrite, sped);
    //     }
    //   }
    //   headWrite();
      
    //   var elem = document.getElementById('demo');
    //   var typeId = elem.getAttribute('data-desc-type');
    //   var i = 0;
    //   var txt =  typeId;
    //   var speed = 50;
      
    //   function typeWriter() {
    //     if (i < txt.length) {
    //       document.getElementById("demo").innerHTML += txt.charAt(i);
    //       i++;
    //       setTimeout(typeWriter, speed);
    //     }
    //   }
    //   typeWriter();
    

      function headerMenu() {

        const $header = $('.header');
  
        const height = $('.coversection').height();
    
          const top = $(window).scrollTop();
      
          if (top >= 200){
      
              if( !$header.hasClass('sticky') ){
                  $header.addClass('sticky')
              }
          }
          else {
              if( $header.hasClass('sticky') ){
                  $header.removeClass('sticky')
              }
          }
        
  
          
   
  
    
    }
      $(window).scroll(function(){
        
        headerMenu();
  
        
      });


    


    
        



        
      
  });


  
  function initMap  ()
  {
  
      if (!document.getElementById('map')){
          return;
      }
  
      var coordMapCenter = {lat: 22.5753421, lng: 88.4276745};
      var coordMarker = {lat: 22.5753421, lng: 88.4276745};
  
      var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 14,
          center: coordMapCenter,
          scrollwheel: true,
          draggable: false,
          styles: [
            {
                "featureType": "administrative.locality",
                "elementType": "all",
                "stylers": [
                    {
                        "hue": "#ff0200"
                    },
                    {
                        "saturation": 7
                    },
                    {
                        "lightness": 19
                    },
                    {
                        "visibility": "on"
                    }
                ]
            },
            {
                "featureType": "administrative.locality",
                "elementType": "labels.text",
                "stylers": [
                    {
                        "visibility": "on"
                    },
                    {
                        "saturation": "-3"
                    }
                ]
            },
            {
                "featureType": "administrative.locality",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#748ca3"
                    }
                ]
            },
            {
                "featureType": "landscape",
                "elementType": "all",
                "stylers": [
                    {
                        "hue": "#ff0200"
                    },
                    {
                        "saturation": -100
                    },
                    {
                        "lightness": 100
                    },
                    {
                        "visibility": "simplified"
                    }
                ]
            },
            {
                "featureType": "poi",
                "elementType": "all",
                "stylers": [
                    {
                        "hue": "#ff0200"
                    },
                    {
                        "saturation": "23"
                    },
                    {
                        "lightness": "20"
                    },
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "poi.school",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "color": "#ffdbda"
                    },
                    {
                        "saturation": "0"
                    },
                    {
                        "visibility": "on"
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "geometry",
                "stylers": [
                    {
                        "hue": "#ff0200"
                    },
                    {
                        "saturation": "100"
                    },
                    {
                        "lightness": 31
                    },
                    {
                        "visibility": "simplified"
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "geometry.stroke",
                "stylers": [
                    {
                        "color": "#f39247"
                    },
                    {
                        "saturation": "0"
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "labels",
                "stylers": [
                    {
                        "hue": "#008eff"
                    },
                    {
                        "saturation": -93
                    },
                    {
                        "lightness": 31
                    },
                    {
                        "visibility": "on"
                    }
                ]
            },
            {
                "featureType": "road.arterial",
                "elementType": "geometry.stroke",
                "stylers": [
                    {
                        "visibility": "on"
                    },
                    {
                        "color": "#ffe5e5"
                    },
                    {
                        "saturation": "0"
                    }
                ]
            },
            {
                "featureType": "road.arterial",
                "elementType": "labels",
                "stylers": [
                    {
                        "hue": "#bbc0c4"
                    },
                    {
                        "saturation": -93
                    },
                    {
                        "lightness": -2
                    },
                    {
                        "visibility": "simplified"
                    }
                ]
            },
            {
                "featureType": "road.arterial",
                "elementType": "labels.text",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "road.local",
                "elementType": "geometry",
                "stylers": [
                    {
                        "hue": "#ff0200"
                    },
                    {
                        "saturation": -90
                    },
                    {
                        "lightness": -8
                    },
                    {
                        "visibility": "simplified"
                    }
                ]
            },
            {
                "featureType": "transit",
                "elementType": "all",
                "stylers": [
                    {
                        "hue": "#e9ebed"
                    },
                    {
                        "saturation": 10
                    },
                    {
                        "lightness": 69
                    },
                    {
                        "visibility": "on"
                    }
                ]
            },
            {
                "featureType": "water",
                "elementType": "all",
                "stylers": [
                    {
                        "hue": "#e9ebed"
                    },
                    {
                        "saturation": -78
                    },
                    {
                        "lightness": 67
                    },
                    {
                        "visibility": "simplified"
                    }
                ]
            }
        ],
          scaleControl: true,
          streetViewControl: true,
          streetViewControlOptions: {
              position: google.maps.ControlPosition.RIGHT_BOTTOM
          },
          zoomControl: true,
          zoomControlOptions: {
              position: google.maps.ControlPosition.RIGHT_BOTTOM
          },
          panControl: true,
  
  
          fullscreenControl:true,
          fullscreenControlOptions:{
              position:google.maps.ControlPosition.LEFT_BOTTOM
          }
      });
  
  
      var officeAddress = '<div id="iw-content">'+
          '<div id="iw-notice">'+
          '</div>'+
          '<div id="firstHeading" class="firstHeading">UFLIX DESIGN, sec-5, kolkata</div>'+
          '<div id="iw-bodyContent">'+
          '</div>'+
          '</div>';
  
      var infowindow = new google.maps.InfoWindow({
          content: officeAddress
      });
  
  
      var mapmarker = new google.maps.Marker({
          position: coordMarker,
          map: map,
          title: 'UFLIX DESIGN '
      });
  
      mapmarker.addListener('click', function() {
          infowindow.open(map, mapmarker);
      });
  
  }