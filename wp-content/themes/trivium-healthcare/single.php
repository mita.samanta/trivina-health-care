<?php 
/*
* 
*/
get_header();
?>



	<div class="shadow"></div>

	<div class="post-wrapper-top clearfix">
		<div class="container">
			<div class="col-lg-12">
				<h2><?php the_title(); ?></h2>
                <!-- <ul class="breadcrumb pull-right">
                    <li><a href="index-2.html">Home</a></li>
                    <li>Pages</li>
                </ul> -->
			</div>
		</div>
	</div><!-- end post-wrapper-top -->





    <div class="white-wrapper">
    	<div class="container">
                    
    		<div id="content" class="col-lg-8 col-md-12 col-sm-8 col-xs-12">

                <div class="row">
                    <div class="col-12 single-department">
                        <div id="aboutslider" class="flexslider clearfix">
                            <ul class="slides">
                                <?php $collects= get_field('service_image');
                                foreach ($collects as $collect ) :
                                    
                                    ?>
                                
                                <li>
                                    <a class="plan" data-lightbox="gallery" href="<?php echo $collect['image-view']['url']?>">
                                        <img src="<?php echo $collect['image-view']['url'] ?>" alt="" class=" depart-single img-responsive alignleft">
                                    </a>
                                    
                                </li>
                                <?php endforeach;?>
                            </ul><!-- end slides -->
                            
                        </div><!-- end slider -->
                        
                    </div>
                    <div class="col-12">
                    
                        <p><?php the_content(); ?> </p>
                    </div>
                </div>
				
					

				
            </div><!-- end content -->
            

            
    		<div id="sidebar" class="col-lg-4 col-md-4 col-sm-4 col-xs-12 department-sidebar">
            	<div class="widget">
                	<div class="title">
                    	<h2>All SERVICES</h2>
                    </div><!-- end title -->
                    <?php
                            $args = array(
                            'post_type' => 'post',
                            'orderby' => 'date' ,
                            'order' => 'DESC' ,
                            
                            'cat'         => 'Services',
                            'paged' => get_query_var('paged'),
                            'post_parent' => $parent
                            );
                            $the_query = new WP_Query($args); ?>
                            <?php if ( $the_query->have_posts() ) : ?>
                            <?php while ( $the_query->have_posts() ) : $the_query->the_post();
                            // var_dump($post); exit();
                            // $categories = get_the_terms($post->ID,'event_categories'); ?>
                    <div class="row">
                        <div class="col-lg-3 col-md-6">
                                <ul class="recent_posts_widget">
                                
                                <li>
                                
                                <a href="<?php the_permalink() ?>">
                                <?php $collects= get_field('service_image');
                                        $collect11=array_shift($collects); 
                                            
                                            ?>
                                <img src="<?php echo $collect11 ['image-view']['url'] ?>" alt="" />
                                
                                </a>
                                
                                
                                </li>
                                
                            </ul><!-- recent posts --> 
                        </div>
                        <div class="col-6">
                        <p class="head-name"><?php the_title(); ?></p>
                        <a class="readmore_widget" href="<?php the_permalink() ?>"><p>Click Here</p></a>
                        </div>
                    </div>
                    <?php endwhile; ?>
                                <?php wp_reset_postdata(); ?>
                            <?php else : ?>
                                <p><?php esc_html_e( 'Sorry, no service found.' ); ?></p>
                            <?php endif; ?>
					
                </div><!-- end widget -->
                
            	
                
            	
            </div><!-- end sidebar -->
    	</div><!-- end container -->
    </div><!-- end white-wrapper -->







	
    
   <?php get_footer();?>